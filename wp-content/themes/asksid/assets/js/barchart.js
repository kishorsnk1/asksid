$(document).ready(function () {
    var inrInvestmentObj = {
            "0": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            "1":[17.91,17.74,17.56,17.38,17.21,17.04,16.87,16.70,16.53,16.36],
            "2":[35.83,35.47,35.12,34.76,34.42,34.07,33.73,33.39,33.06,32.73],
            "3":[53.74,53.21,52.67,52.15,51.63,51.11,50.60,50.09,49.59,49.09],
            "4":[71.66,70.94,70.23,69.52,68.83,68.15,67.46,66.79,66.12,65.46],
            "5":[89.57,88.68,87.79,86.91,86.04,85.18,84.33,83.49,82.65,81.82],
            "6":[107.49,106.41,105.35,104.29,103.25,102.21,101.20,100.18,99.18,98.19],
            "7":[125.40,124.14,122.90,121.68,120.46,119.25,118.06,116.88,115.71,114.55],
            "8":[143.31,141.88,140.46,139.06,137.67,136.29,134.93,133.58,132.24,130.92],
            "9":[161.23,159.62,158.02,156.44,154.88,153.33,151.79,150.28,148.77,147.28],
            "10":[179.14,177.35,175.58,173.82,172.08,170.36,168.66,166.97,165.30,163.65]
    }
    var dollarInvestmentObj = {
        "0": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "1":[26871,26603,26337,26073,25813,25554,25299,25046,24795,24547],
        "2":[53743,53205,52673,52147,51625,51109,50598,50092,49591,49095],
        "3":[80614,79808,79010,78220,77438,76663,75897,75138,74386,73642],
        "4":[107486,106411,105347,104293,103250,102218,101196,100184,99182,98190],
        "5":[134357,133013,131683,130366,129063,127772,126494,125229,123977,122737],
        "6":[161228,159616,158020,156440,154875,153327,151793,150275,148773,147285],
        "7":[179143,177351,175578,173822,172084,170363,168659,166973,165303,163650],
        "8":[206014,203954,201914,199895,197896,195917,193958,192019,190098,188197],
        "9":[232885,230557,228251,225968,223709,221472,219257,217064,214894,212745],
        "10":[259757,257159,254588,252042,249521,247026,244556,242110,239689,237292]
    }
    var selectedCurrencyType = $('input[name="optradio"]:checked').val();
    var sliderValue = '1';
    switchRangeSliderFn(selectedCurrencyType);
    switchCanvasFn(selectedCurrencyType);
    setTimeout(function(){
        createChartFn(selectedCurrencyType, sliderValue);
    },100)

    $('#ex1').slider({
        formatter: function (value) {
            $('.rupeesRangeSlider .slider-handle').text(value);
            sliderValue = value;
            return value + ' Cr.';
        }
    });
    $('#ex1').on('change', function () {
        createChartFn(selectedCurrencyType, sliderValue);
    });
    $('#ex2').slider({
        formatter: function (value) {
            sliderValue = value;
            var fortooltipvaluearray = ['0','150000','300000','450000','600000','750000','900000','1000000','1150000','1300000','1450000'];
            var showvalue = fortooltipvaluearray[value];
            var trimedvalue = showvalue.split('');
            trimedvalue.splice((trimedvalue.length-3), 3, 'k');
            trimedvalue = trimedvalue.join('');
            $('.dollarsRangeSlider .slider-handle').text(trimedvalue);
            return '$ ' + showvalue;
        }
    });
    $('#ex2').on('change', function () {
        createChartFn(selectedCurrencyType, sliderValue);
    });
    $('input[name="optradio"]').on('change', function () {
        selectedCurrencyType = $(this).val();
        switchCanvasFn($(this).val());
        switchRangeSliderFn($(this).val());
        $('.switch-to').toggleClass('rupees dollars');

        createChartFn(selectedCurrencyType, sliderValue);
    });

    function createChartFn(currencytype, rangeSliderValue) {
        var dataArray = loadArrayOfValuesFn(rangeSliderValue, currencytype);
        switchCanvasFn(currencytype);
        var totalofAllYears = dataArray.reduce(function(sum, curval){
            return sum + curval;
        });
        // totalofAllYears = currencytype === 'rupees'?((Math.round((totalofAllYears + Number.EPSILON) * 100) / 100)/100).toFixed(2):totalofAllYears;
        totalofAllYears = currencytype === 'rupees'?(totalofAllYears/100).toFixed(2):totalofAllYears;
        $('#totalOfAllYears').html("".concat(currencytype === 'rupees' ? '₹' : '$', " ").concat(totalofAllYears.toLocaleString('en')).concat(currencytype === 'rupees' ? '<small class="bold"> Cr.</small>' : ''));
        var ctx = document.getElementById(currencytype + "Chart");
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["EOY-01", "EOY-02", "EOY-03", "EOY-04", "EOY-05", "EOY-06", "EOY-07", "EOY-08", "EOY-09", "EOY-10"],
                datasets: [{
                    label: '# of DE Energy',
                    data: dataArray,
                    backgroundColor: [
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)'
                    ],
                    borderColor: [,
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)',
                        'rgba(97, 197, 133, 1)'
                    ],
                    borderWidth: 0
                }]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            if(currencytype !== 'rupees'){
                                var value = data.datasets[0].data[tooltipItem.index];
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                value = value.join(',');
                                return value;
                            }else{
                                return tooltipItem.yLabel;
                            }
                        }
                    }
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        // ticks: {
                        //     maxRotation: 90,
                        //     minRotation: 80
                        // },
                        gridLines: {
                            display: false
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Years'
                        }
                    // },
                    // {
                    //     position: "top",
                    //     ticks: {
                    //         maxRotation: 90,
                    //         minRotation: 80
                    //     },
                    //     gridLines: {
                    //         offsetGridLines: false // et matcher pareil ici
                    //     }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false
                        },
                        scaleLabel: {
                            display: true,
                            labelString: currencytype === 'rupees'?'Dividend (INR/lacs)':'Dividend (USD)'
                        },
                        ticks: {
                            beginAtZero: true,
                            maxTicksLimit: 9,
                            userCallback: function(value, index, values) {
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                value = value.join(',');
                                return value;
                            }
                        }
                    }]
                }
            }
        });
        
        function loadArrayOfValuesFn(slidedValue, currencytype) {
            var activeObject = currencytype === 'rupees' ? inrInvestmentObj : dollarInvestmentObj;
            var sliderElm = currencytype === 'rupees' ? 'rupeesRangeSlider' : 'dollarsRangeSlider';
            var num = Number($('.'+sliderElm + ' '+'input[data-slider-id="ex1Slider"]').val());
            // return activeObject[slidedValue];
            return activeObject[num];
        }
    }
    function switchCanvasFn(selectedCurrencyType) {
        $('.myChart').remove();
        var divtag = selectedCurrencyType === 'rupees' ? '<canvas id="rupeesChart" class="mychart"></canvas>' : '<canvas id="dollarsChart" class="mychart"></canvas>';
        $('.chart-container').html(divtag);
    }
    function switchRangeSliderFn(selectedCurrencyType) {
        $('.range-slider-block').hide();
        $('.range-slider-block.' + selectedCurrencyType + 'RangeSlider').show();
    }
});