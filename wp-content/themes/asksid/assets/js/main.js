$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 80) {
        //clearHeader, not clearheader - caps H
        $(".headers").addClass("sticky");
    }else{
      $(".headers").removeClass("sticky");
    }
}); //missing );
// window.onscroll = function() {myFunction1()};

// var header = document.getElementById("headers-mobile");
// var sticky = header.offsetTop;

// function myFunction1() {
//   if (window.pageYOffset > sticky) {
//     header.classList.add("sticky");
//   } else {
//     header.classList.remove("sticky");
//   }
// }

/*
$(document).ready(function () {
    $('header').addClass('tSticky');
    //$('#navbarsExample05').height($('.desktop-ftop .navbar').height() + 10);
    var fire = 0;
    var defScr = 1;
    if ($(window).width() > 991) {
        $('.bg-for-first-section').height($('.very-first-section').height());
        setTimeout(function () {
            $('.bg-for-first-section').css({ 'top': ($('.desktop-ftop').height() * 2) - 17 });
            $('.very-first-section').css({ 'margin-top': $('.desktop-ftop').height() });
        }, 500)
    } else {
        if ($(window).width() > 768) {
            $('.bg-for-first-section').height(($('.very-first-section').height() / 2) + 150);
            setTimeout(function () {
                $('.bg-for-first-section').css({ 'top': $('.desktop-ftop').height() + 30 });
                $('.very-first-section').css({ 'margin-top': $('.desktop-ftop').height() });
            }, 500)
        } else {
            if ($(window).width() > 575) {
                $('.bg-for-first-section').height($('.very-first-section').height() / 1.22);
            } else {
                $('.bg-for-first-section').height($('.very-first-section').height());
            }
            setTimeout(function () {
                $('.bg-for-first-section').css({ 'top': $('.mobile-ftop').height() + 30 });
                $('.very-first-section').css({ 'margin-top': $('.mobile-ftop').height() });
            }, 500)
        }
    }

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 400) {
            if (fire == 0) {
                $('.bg-for-first-section').fadeOut("fast");
            }
            fire = 1;
        } else {
            $('.bg-for-first-section').fadeIn("slow");
            fire = 0;
        }
        var scroll = $(window).scrollTop();

        if (scroll >= defScr) {
            $("header").removeClass("tSticky");
        } else {
            $("header").addClass("tSticky");
        }
    });
});*/

$(document).ready(function () {
    $("ul#menu-header-menu li.dropdown a").addClass("dropdown-toggle");
    $(".dropdown-menu li a").removeClass("dropdown-toggle");

    setTimeout(function(){
        $(".popproject a").attr({"data-toggle":"modal", "data-target":"#listproject"});
        $(".popmodal a").attr({"data-toggle":"modal", "data-target":"#invest"});
    },100);
});

// Mobile Sliding Menu
$(document).ready(function () {
    $('.navbar-hamburg').click(function () {
        $checkClass = $('.slidingMenu').attr("class").split(' ').pop();
        //alert($checkClass);
        if ($checkClass === 'slidingMenu_hide') {
            $('.slidingMenu').addClass('slidingMenu_show').removeClass('slidingMenu_hide');
            $('.menuOverlay').css("display", "block");
        } else {
            $('.slidingMenu').addClass('slidingMenu_hide').removeClass('slidingMenu_show');
            $('.menuOverlay').css("display", "none");
        }
    });
});

// Number Speak
var a = 0;
$(window).scroll(function() {

  var oTop = $('#counter').offset().top - window.innerHeight;
  if (a == 0 && $(window).scrollTop() > oTop) {
    $('.counter-value').each(function() {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
          countNum: countTo
        },

        {

          duration: 2000,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
    });
    a = 1;
  }

});

// Click popup

$(document).ready(function(){
    $('.closePopup').click(function(){
        $('.fullproject,.business,.clarity,.knowledge,.research,.vendor').css('display','none');
    });

    $('#fullproject').click(function(){
        $('.fullproject').css('display','block');
    });

    $('#business').click(function(){
        $('.business').css('display','block');
    });

    $('#clarity').click(function(){
        $('.clarity').css('display','block');
    });

    $('#knowledge').click(function(){
        $('.knowledge').css('display','block');
    });

    $('#research').click(function(){
        $('.research').css('display','block');
    });

    $('#vendor').click(function(){
        $('.vendor').css('display','block');
    });
});

// Timeline
$(document).ready(function(){
    $(".marker").click(function() {
        $('.marker').removeClass('active');
        $(this).addClass('active');
    });

    $(".mfirst").click(function() {
        $('#v1').css('display','block');
          $('#v2').css('display','none');
          $('#v3').css('display','none');
          $('#v4').css('display','none');
          $('#v5').css('display','none');
    });
    $(".m2").click(function() {
        $('#v2').css('display','block');
          
          $('#v1').css('display','none');
          $('#v3').css('display','none');
          $('#v4').css('display','none');
          $('#v5').css('display','none');
    });
    $(".m3").click(function() {
        $('#v3').css('display','block');

          $('#v1').css('display','none');
          $('#v2').css('display','none');
          $('#v4').css('display','none');
          $('#v5').css('display','none');
    });
    $(".m4").click(function() {
        $('#v4').css('display','block');
         
          $('#v1').css('display','none');
          $('#v2').css('display','none');
          $('#v3').css('display','none');
          $('#v5').css('display','none');
    });
    $(".mlast").click(function() {
        $('#v5').css('display','block');
         
          $('#v1').css('display','none');
          $('#v2').css('display','none');
          $('#v3').css('display','none');
          $('#v4').css('display','none');
    });

});

$(document).ready(function(){
  
  $('.whySIdUl li a').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.whySIdUl li a').removeClass('active');
    $('.quotewhiteText').removeClass('active');

    $(this).addClass('active');
    $("#"+tab_id).addClass('active');
  })

  $('.whySIdUl li a').mouseover(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.whySIdUl li a').removeClass('active');
    $('.quotewhiteText').removeClass('active');

    $(this).addClass('active');
    $("#"+tab_id).addClass('active');
  })


  $(document).ready(function() {
    $('.didyouknowOWL').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      autoplay:true,
autoplayTimeout:5000,
autoplayHoverPause:false,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
          loop: true,
          margin: 20
        }
      }
    })
  })

  $(document).ready(function() {
    $('.whysidOWL').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      autoplay:true,
autoplayTimeout:5000,
autoplayHoverPause:false,
dots: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
          loop: true,
          margin: 20
        }
      }
    })
  })

})