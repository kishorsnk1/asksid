<?php
/**
 * Template Name: Blog
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */
global $post; 
get_header();
?>
	<?php include('template-parts/inner-banner.php'); ?>
	<h5 class="floatingTitle">Blogs</h5>
<?php
	$the_query= new WP_Query(array(
        'post_type'=>'blog',
        'posts_per_page' => 1,
        'paged' => $paged,
        'orderby'   => 'ID',
        'order' => 'DESC',
        'tax_query' => array(
	        array(
	            'taxonomy' => 'editors_pick',
	            'field' => 'slug',
	            'terms' => 'editor-pick'
	            
	        ))
    ));
if($the_query->have_posts()) :
    while($the_query->have_posts())  : $the_query->the_post();
?>

<main>
	
	<section class="editorsPick global">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="blogEditorImage">
						<?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('', array('class' => 'img-fluid'));
                            }
                        ?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="blogEditorContent">
						<small>EDITORS PICK</small>
						<h2><?php the_title(); ?></h2>
						<?php the_excerpt(); ?>
						<?php
						$users = get_field("authors");

						if( $users ): ?>
						<ul class="authors-list editorpick_list">
						    <?php $i=0;foreach( $users as $user ): ?>
						        <li>
						            <div class="sImage1">
						            	<?php $image = get_field('profile_pic', 'user_'.$user['ID']); ?>
						            	<img src="<?php echo $image['url']; ?>" alt="<?php echo $user['user_firstname']; ?>" class="img-fluid" />
						            </div>
									<div class="sAuth">
										<span><strong><?php echo $user['user_firstname'] . $user['user_lastname']; ?></strong></span>
										<span><?php echo get_field('designation', 'user_'.$user['ID']).', '. get_field('company', 'user_'.$user['ID']); ?></span>
									</div>
						        </li>
						    <?php $i++;endforeach; ?>
						</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<section class="subscribe global">
  <div class="container mb-5">
     <div class="row m-0">
        <div class="col-lg-7 col-md-7 col-12">
          <div class="subscribe-head">
            <h2><?php echo get_field('subscribe_text','option'); ?></h2>
          </div>
        </div>
        <div class="col-lg-5 col-md-5 col-12">
          <div class="subscribe-box">
          	<?php echo do_shortcode('[contact-form-7 id="177" title="Subscribe"]'); ?>
            <!-- <input type="email" placeholder="Enter email ID" name="email" required class="subscribe-input">
              <div class="read-more btn mt-4 float-right">
                    <a href="" class="button-open-small button-white">Subscribe</a>
              </div> -->
          </div>
        </div>
      </div>
  </div>
</section>

<?php
 endwhile; 
    else :
        echo '<h2 class="blogsNotFound">No Blogs Found</h2>';  
    endif; 
    wp_reset_postdata(); 
    
?>
<div class="listBlog">
	<div class="container">
		<div class="row">
			
			<?php
				$the_query = new WP_Query(array(
			        'post_type'=>'blog',
			        'posts_per_page' => 2,
			        'paged' => $paged,
			        'orderby'   => 'ID',
			        'order' => 'DESC'
			    ));
				if($the_query->have_posts()) :
			    while($the_query->have_posts())  : $the_query->the_post();
			?>
				<div class="col-sm-6">
					<div class="singleListBlog" onclick="myhref('<?php echo get_permalink(); ?>');">
						
						<div class="singleListBlogImage">
							<?php
	                            if ( has_post_thumbnail() ) {
	                                the_post_thumbnail('', array('class' => 'img-fluid'));
	                            }
	                        ?>
	                    </div>
                        <div class="singleListBlogContent">
	                        <h2><?php the_title(); ?></h2>
	                        <div class="row">
	                        	<div class="col-4">
	                        		<p> -Neha M</p>
	                        	</div>
	                        	<div class="col-8">
	                        		<div class="sBlogSocial float-right">
										<span>Share : </span>
										<a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
										  <i class="fa fa-facebook"></i>
										</a>
										<a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
										  <i class="fa fa-twitter"></i>
										</a>	
										<a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
										  <i class="fa fa-linkedin"></i>
										</a>
									</div>
	                        	</div>
	                        </div>     
	                    </div> 

					</div>
				</div>
			<?php
			 endwhile; 
			    else :
			        echo '<h2 class="blogsNotFound">No Blogs Found</h2>';  
			    endif; 
			    wp_reset_postdata(); 
			    
			    echo do_shortcode('[ajax_load_more container_type="div" button_label="More Blogs" post_type="blog" posts_per_page="10" offset="2" placeholder="true" scroll_container="row" transition_container_classes="row p-3" button_loading_label="Loading....."]');
			?>
		</div>
	</div>
</div>
</main>






<?php
get_footer();?>
<script type="text/javascript">
    function myhref(web){
      window.location.href = web;}
</script>