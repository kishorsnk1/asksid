<div class="col-sm-6">
                    <div class="singleListBlog" onclick="myhref('<?php echo get_permalink(); ?>');">
                        
                        <div class="singleListBlogImage">
                            <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('', array('class' => 'img-fluid'));
                                }
                            ?>
                        </div>
                        <div class="singleListBlogContent">
                            <h2><?php the_title(); ?></h2>
                            <div class="row">
                                <div class="col-4">
                                    <p> -Neha M</p>
                                </div>
                                <div class="col-8">
                                    <div class="sBlogSocial float-right">
                                        <span>Share : </span>
                                        <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                          <i class="fa fa-facebook"></i>
                                        </a>
                                        <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                          <i class="fa fa-twitter"></i>
                                        </a>    
                                        <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                          <i class="fa fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>     
                        </div> 

                    </div>
                </div>