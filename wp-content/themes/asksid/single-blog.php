<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package chek
 */

get_header();
wpb_set_post_views(get_the_ID());

$Ad1 = get_field('ads_image_1');
$Ad2 = get_field('ads_image_2');
$Ad3 = get_field('ads_image_3');
?>

	<?php include('template-parts/inner-banner.php'); ?>
	<h5 class="floatingTitle">Blogs</h5>
<main class="sBlog global">
	<section class="sBlogFirst global">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<h1 class="blogSingleTitle"><?php the_title(); ?></h1>
					<small><?php echo get_the_date(); ?></small>
					<?php
					$users = get_field("authors");

					if( $users ): ?>
					<ul class="authors-list">
					    <?php $i=0;foreach( $users as $user ): ?>
					        <li>
					            <div class="sImage1">
					            	<?php $image = get_field('profile_pic', 'user_'.$user['ID']); ?>
					            	<img src="<?php echo $image['url']; ?>" alt="<?php echo $user['user_firstname']; ?>" class="img-fluid" />
					            </div>
								<div class="sAuth">
									<span><strong><?php echo $user['user_firstname'] . $user['user_lastname']; ?></strong></span>
									<span><?php echo get_field('designation', 'user_'.$user['ID']).', '. get_field('company', 'user_'.$user['ID']); ?></span>
								</div>
					        </li>
					    <?php $i++;endforeach; ?>
					</ul>
					<?php endif; ?>

					<?php
						if ( has_post_thumbnail() ) {
                            the_post_thumbnail('', array('class' => 'img-fluid mb-3'));
                        }
					?>
					<div class="sBlogContent">
						<?php the_content(); ?>
					</div>
					<div class="sBlogSocial">
						<div class="row">
						<div class="col-6">
							<span>Share : </span>
							<a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
							  <i class="fa fa-facebook"></i>
							</a>
							<a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
							  <i class="fa fa-twitter"></i>
							</a>	
							<a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
							  <i class="fa fa-linkedin"></i>
							</a>
						</div>
						<div class="col-6">
						<div class="sBlogNavigate">
							<a href="" class="prevArr"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous </a> |
							<a href="" class="nextArr">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
						</div>
						</div>
						</div>
					</div>
					<div class="sBlogAdsThree mt-3">
						<img src="<?php echo $Ad3['url']; ?>" class="img-fluid" alt="<?php echo $Ad3['alt']; ?>" />
					</div>
				</div>
				<div class="col-sm-3">
					<div class="sBlogSidebar">
						<div class="sBlogCategory">
							<?php
								$terms = get_terms( array(
								    'taxonomy' => 'blog_categories',
								    'hide_empty' => false,
								) );
							?>
							<h6>Top Categories</h6>
							<ul class="topCategories">
								<?php foreach($terms as $term){ ?>
								<li>
									<a href="<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
								</li>
								<?php } ?>
							</ul>
						</div>
						<div class="sBlogSubscribe mt-3">
							
						</div>
						<div class="sBlogAdsOne mt-3">
							<img src="<?php echo $Ad1['url']; ?>" class="img-fluid" alt="<?php echo $Ad1['alt']; ?>" />
						</div>
						<div class="sBlogTrendingArticles sBlogCategory mt-3">
							<h6 class="mb-3">Trending Articles</h6>
							<ul class="topCategories">
							<?php 
							$popularpost = new WP_Query( array('post_type'=>'blog', 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
							while ( $popularpost->have_posts() ) : $popularpost->the_post();
							 ?>
							<li>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</li>
							 <?php
							endwhile;
							?>
							</ul>
						</div>
						<div class="sBlogAdsTwo mt-3">
							<img src="<?php echo $Ad2['url']; ?>" class="img-fluid" alt="<?php echo $Ad2['alt']; ?>" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="sBlogSocial floatingSocialShare">
	<span>Share : </span>
		<a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
		  <i class="fa fa-facebook"></i>
		</a>
		<a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
		  <i class="fa fa-twitter"></i>
		</a>	
		<a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
		  <i class="fa fa-linkedin"></i>
		</a>

</div>
</main>

<?php
get_footer();
