<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package chek
 */

get_header();
?>

<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<center>
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php //esc_html_e( 'Oops! That page can&rsquo;t be found.', 'Distributed_Energy' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/404.png" alt="404 Not Found" class="img-fluid" />
					
				</div><!-- .page-content -->
			</section><!-- .error-404 -->
			</center>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>	

<?php
get_footer();
