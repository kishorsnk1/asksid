<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */

$footer_logo = get_field('footer_logo', 'option');
?>
<footer class="global">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<h2><?php the_field('footer_menu_1','option'); ?></h2>
				<?php wp_nav_menu( array( 'theme_location' => 'footermenu1', 'menu' => 'conditional-menu-name', 'menu_class' => 'nav-menu' ) ); ?>
				<ul class="ulFooterSocial">
						<?php

							// Check rows exists.
							if( have_rows('social_media','option') ):

							    // Loop through rows.
							    while( have_rows('social_media','option') ) : the_row();

							        // Load sub field value.
							        $social_media_icon_class = get_sub_field('social_media_icon_class');
							        $social_media_url = get_sub_field('social_media_url');
							        // Do something...
						?>
								<li>
									<a target="_blank" href="<?php echo $social_media_url; ?>"><i class="<?php echo $social_media_icon_class; ?>" aria-hidden="true"></i></a>
								</li>
						<?php
							    // End loop.
							    endwhile;

							// No value.
							else :
							    // Do something...
							endif;
						?>
					</ul>
					<?php wp_nav_menu( array( 'theme_location' => 'footermenu2', 'menu' => 'conditional-menu-name', 'menu_class' => 'termsCondition-menu' ) ); ?>
			</div>
			<!-- <div class="col-sm-2">
				<h2><?php //the_field('footer_menu_2','option'); ?></h2>
				<?php //wp_nav_menu( array( 'theme_location' => 'footermenu2', 'menu' => 'conditional-menu-name', 'menu_class' => 'nav-menu' ) ); ?>
			</div>
			<div class="col-sm-2">
				<h2><?php //the_field('footer_menu_3','option'); ?></h2>
				<?php //wp_nav_menu( array( 'theme_location' => 'footermenu3', 'menu' => 'conditional-menu-name', 'menu_class' => 'nav-menu' ) ); ?>
			</div>
			<div class="col-sm-2">
				<h2><?php //the_field('footer_menu_4','option'); ?></h2>
				<?php //wp_nav_menu( array( 'theme_location' => 'footermenu4', 'menu' => 'conditional-menu-name', 'menu_class' => 'nav-menu' ) ); ?>
			</div> -->
			<div class="col-sm-6">
				<div class="footercontent">
					<p><?php the_field('small_description','option'); ?></p>
					<button class="button-close-black mb-3" data-toggle="modal" data-target="#bookDemo">schedule a demo</button>
					<strong>Phone: <?php the_field('phone_number','option'); ?></strong>
					<strong>Email: <?php the_field('email_id','option'); ?></strong>
				</div>
			</div>
		</div>
		<div class="subFooter">
			<div class="row">				
				<div class="col-sm-6 copyright">
					<p>© 2016-2021 by AskSid Technology Solutions Private Limited</p>
				</div>
				<div class="col-sm-6 copyrightComp">
					<p>A techstars venture <label>techstars<span class="colorGreen">_</span></label></p>
					
				</div>
			</div>
		</div>		
	</div>
</footer>

<!-- The Modal -->
<div class="modal" id="bookDemo">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Book a demo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
        	<?php echo do_shortcode('[contact-form-7 id="466" title="Book a Demo"]'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
