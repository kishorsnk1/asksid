<?php
   /**
    * Template Name: Terms and conditions
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package chek
    */
   global $post; 
   get_header();
   ?>
<?php //include get_template_directory().'/template-parts/inner-banner.php'; ?>
<h5 class="floatingTitle"><?php the_title(); ?></h5>
<?php
   // $banner_image_car = get_field('banner_image_car');
   // $banner_text_car = get_field('banner_text_car');
   ?>
<main class="headerResource">
   <div class="innerBanner">
      <img src="<?php echo get_template_directory_uri();?>/assets/images/inner_banner.png" alt="Banner" class="img-fluid d-none d-sm-block">
      <img src="<?php echo get_template_directory_uri();?>/assets/images/innerbannermobile.png" alt="Banner" class="img-fluid d-block d-sm-none">
   </div>
</main>

<section class="termsContent">
 <div class="container">
  <div class="row">
    <div class="col-12">
      <div class="contentStarts">  
        <?php the_content(); ?>
    </div>
  </div>
 </div>
</section> 
            
<?php
get_footer();?>
