<?php
/**
 * Template Name: Contact Us
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */
global $post; 
get_header();
?>
<?php //include get_template_directory().'/template-parts/inner-banner.php'; ?>
	<h5 class="floatingTitle"><?php the_title(); ?></h5>
<?php
	$banner_image_contact = get_field('banner_image_contact');
	$banner_text_contact = get_field('banner_text_contact');
?>

<main class="headerWave">
	<section class="">
		<img src="<?php echo $banner_image_contact['url']; ?>" alt="<?php echo $banner_image_contact['alt']; ?>" class="img-fluid moveUpBanner">
					<h1 class="moveUpBannerText"><?php echo $banner_text_contact; ?></h1>
	</section>
</main>



<?php

// check if the flexible content field has rows of data
if( have_rows('contact_info') ):

 	// loop through the rows of data
    while ( have_rows('contact_info') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'contact_info_section' ):
        	$section_title = get_sub_field('section_title');
        	$form_shortcode = get_sub_field('form_shortcode');
        	
        	$right_section_title = get_sub_field('right_section_title');
        	$right_section_email_id_text = get_sub_field('right_section_email_id_text');
        	$right_section_email_id = get_sub_field('right_section_email_id');
        	$right_section_image = get_sub_field('right_section_image');
        	$right_section_person_name = get_sub_field('right_section_person_name');
        	$right_section_designation = get_sub_field('right_section_designation');
        	$right_section_text = get_sub_field('right_section_text');
        	$right_section_description = get_sub_field('right_section_description');
?>
	<section class="contact_info global">
		<div class="container">			
			<div class="row">
			<div class="col-sm-6">
						<h2 class="fw-700"><?php echo $section_title; ?></h2>
					<?php echo do_shortcode($form_shortcode); ?>
				</div>
				<div class="col-sm-6">
					<h6 class="colorRed"><?php echo $right_section_title; ?></h6>
					<h5><?php echo $right_section_email_id_text; ?></h5>
					<a class="emailID" href="mailto:<?php echo $right_section_email_id; ?>"><?php echo $right_section_email_id; ?></a>
					<hr>
					<div class="row">
						<div class="col-sm-9">
							<h2><?php echo $right_section_person_name; ?></h2>
							<h3><?php echo $right_section_designation; ?></h3>
							<small><?php echo $right_section_text; ?></small>
						</div>
						<div class="col-sm-3">
							<img class="img-fluid" src="<?php echo $right_section_image['url']; ?>" alt="<?php echo $right_section_image['alt']; ?>">
						</div>
					</div>
					<p class="contactQuote"><?php echo $right_section_description; ?></p>
				</div>
				</div>
		</div>
</section>
<?php
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<!-- The Modal -->
<div class="modal" id="careerPopup">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Join Us</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
        	<?php echo do_shortcode('[contact-form-7 id="210" title="Careers"]'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
get_footer();