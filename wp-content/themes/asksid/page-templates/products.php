<?php
   /**
    * Template Name: Products
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package chek
    */
   global $post; 
   get_header();
   ?>
<?php //include get_template_directory().'/template-parts/inner-banner.php'; ?>
<h5 class="floatingTitle"><?php the_title(); ?></h5>
<?php
   $banner_image_products = get_field('banner_image_products');
   $banner_text_products = get_field('banner_text_products');
   ?>
<main class="headerWave">
   <section class="">
      <img src="<?php echo $banner_image_products['url'];?>" alt="<?php echo $banner_image_products['alt'];?>" class="img-fluid moveUpBanner">
      <h1 class="moveUpBannerText"><?php echo $banner_text_products; ?></h1>
   </section>
</main>

<?php
   $section_one_title_products = get_field('section_one_title_products');
   $section_one_image_desktop_products = get_field('section_one_image_desktop_products');
   $section_one_image_mobile_products = get_field('section_one_image_mobile_products');
?>
<section class="product-overview mt-lg-5 mt-md-5 mt-2">
   <div class="container mb-lg-5 mb-5">
      <div class="row">
         <div class="col-12 col-lg-9 col-md-10 mx-auto text-center mb-3">
            <h2 class="text-black">
               <?php echo $banner_text_products; ?>
            </h2>
         </div>
         <div class="col-12 mt-2 wave-bg">
            <div class="row">
               <div class="d-lg-block d-md-block d-none">
                  <img src="<?php echo $section_one_image_desktop_products['url']; ?>" alt="<?php echo $section_one_image_desktop_products['alt']; ?>" class="img-fluid moveUpBanner">
               </div>
               <div class="d-block d-md-none d-lg-none">
                  <img src="<?php echo $section_one_image_mobile_products['url']; ?>" alt="<?php echo $section_one_image_mobile_products['alt']; ?>" class="img-fluid moveUpBanner">
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


<?php
  if( have_rows('products_list') ):
    while( have_rows('products_list') ) : the_row();

        // Get parent value.
        $image = get_sub_field('image');
        $title = get_sub_field('title');
        $subtitle = get_sub_field('subtitle');
        $product_para_title_1 = get_sub_field('product_para_title_1');
        $product_para_description_1 = get_sub_field('product_para_description_1');
        $product_para_title_2 = get_sub_field('product_para_title_2');
        $product_para_description_2 = get_sub_field('product_para_description_2');
        $product_para_title_3 = get_sub_field('product_para_title_3');
        $product_para_description_3 = get_sub_field('product_para_description_3');
?>
    <section class="products-section my-5">
       <div class="container">
          <div class="row">
             <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
                <div class="row">
                   <div class="col-lg-3 col-md-4 col-12 how-img">
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid moveUpBanner">
                   </div>
                   <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                      <div class="products-section heading-box">
                        <h4><?php echo $title; ?></h4>
                        <p class="subheading"><?php echo $subtitle; ?></p>
                      </div>
                   </div>
                </div>
             </div>
             <div class="col-12">
                <div class="row advantage-row mt-5 px-lg-0 px-2">
                   <div class="col-lg-4 col-md-4 col-12 p-2">
                      <div class="main-desc px-lg-4">
                         <h5 class="maindesc-1"><?php echo $product_para_title_1; ?></h5>
                         <p class="maindesc-1 description"><?php echo $product_para_description_1; ?></p>
                      </div>
                   </div>
                   <div class="col-lg-4 col-md-4 col-12 p-2">
                      <div class="main-desc border-lr px-lg-4 px-md-4">
                         <h5 class="maindesc-1"><?php echo $product_para_title_2; ?></h5>
                         <p class="maindesc-1 description"><?php echo $product_para_description_2; ?></p>
                      </div>
                   </div>
                   <div class="col-lg-4 col-md-4 col-12 p-2">
                      <div class="main-desc px-lg-4">
                         <h5 class="maindesc-1"><?php echo $product_para_title_3; ?></h5>
                         <p class="maindesc-1 description"><?php echo $product_para_description_3; ?></p>
                      </div>
                   </div>
                </div>
             </div>

          </div>
       </div>
       <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list">
                    <ul>
<?php
        // Loop over sub repeater rows.
        if( have_rows('products_points') ):
            while( have_rows('products_points') ) : the_row();

                // Get sub value.
                $name = get_sub_field('name');
                $border_right = get_sub_field('border_right');
?>
                <li class="<?php if($border_right == 'No'){echo 'border-0'; } ?>"><?php echo $name; ?></li>
<?php
            endwhile;
        endif;


?>
  </ul>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
  </section>
<?php
    endwhile;
  endif;

?>
<!-- <section class="products-section my-5">
   <div class="container">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/product-one.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                  <div class="products-section heading-box">
                  	<h4>IntelliSID</h4>
                    <p class="subheading">The core of intelligent conversations</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="row advantage-row mt-5 px-lg-0 px-2">
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Centralise knowledge Base</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc border-lr px-lg-4 px-md-4">
                     <h5 class="maindesc-1">Minimal Effort</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Driving force</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list">
								    <ul>
								        <li>Knowledge base of tweet sized Q&As</li>
								        <li>Tag enriched product data</li>
								        <li class="border-0">Property AI models</li>
								        <li>Automated onboarding process</li>
								        <li class="border-0">Automated onboarding process Automated onboarding process</li>
								    </ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
</section>

<section class="products-section my-7">
   <div class="container">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/product-two.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-5 py-md-5">
                  <div class="products-section heading-box">
                  	<h4>HeySID</h4>
                    <p class="subheading">Assist your customers,wherever they are</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="row advantage-row mt-5 px-lg-0 px-2">
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Retail Expert Chatbot</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc border-lr px-lg-4 px-md-4">
                     <h5 class="maindesc-1">Deployable In Chat Or Voice</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Driving Personalised Engagement</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list">
								    <ul>
								        <li>Accelerated conversions</li>
								        <li>Increased sales conversations</li>
								        <li class="border-0">Increase in self-serviced conversations</li>
								        <li>Reduced call volumes</li>
								        <li class="border-0">Increased agent productivity</li>
								    </ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
</section>

<section class="products-section my-7">
   <div class="container">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/product-three.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-3 py-md-5">
                  <div class="products-section heading-box">
                  	<h4>ReplaySID</h4>
                    <p class="subheading">Adios wait times</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="row advantage-row mt-5 px-lg-0 px-2">
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Seamless Escaltion</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc border-lr px-lg-4 px-md-4">
                     <h5 class="maindesc-1">Replay With Ease</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Consumer Delight</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list">
								    <ul class="px-lg-5">
								        <li>Access to conversion history</li>
								        <li class="border-0">Agent console for easy access</li>
								        <li>Shorter wait times for consumers to seek up</li>
								        <li class="border-0">Increased agent productivity</li>
								    </ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
</section>

<section class="products-section my-7">
   <div class="container">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/product-4.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-3 py-md-5">
                  <div class="products-section heading-box">
                  	<h4>SwitchSID</h4>
                    <p class="subheading">The perfect amalgam of human and AI</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="row advantage-row mt-5 px-lg-0 px-2">
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Intutive Handoff</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc border-lr px-lg-4 px-md-4">
                     <h5 class="maindesc-1">Intelligent Dashboards</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Empowering Agents</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list my-lg-4 py-lg-3 mx-auto">
								    <ul>
								        <li>Seamless hadnoff to human agent</li>
								        <li>Accelerate conversions</li>
								        <li class="border-0">Improove agent productivity</li>
								    </ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
</section>

<section class="products-section my-7">
   <div class="container">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/product-5.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-3 py-md-5">
                  <div class="products-section heading-box">
                  	<h4>TrainSID</h4>
                    <p class="subheading">Continous AI richment</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="row advantage-row mt-5 px-lg-0 px-2">
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">AI Brain's Coach</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc border-lr px-lg-4 px-md-4">
                     <h5 class="maindesc-1">Round the Clock Learning</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1">Training Toolbox</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list my-lg-4 py-lg-3 mx-auto">
								    <ul>
								        <li>AI & ML Algorithms</li>
								        <li>Enrichment of product data</li>
								        <li class="border-0">Unanswered Questions</li>
								    </ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
</section>

<section class="products-section my-7">
   <div class="container">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/product-6.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                  <div class="products-section heading-box">
                  	<h4>MonitorSID</h4>
                    <p class="subheading">Access every conversation in real-time</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="row advantage-row mt-5 px-lg-0 px-2">
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1 color-b">Consumer Data At Your Disposal</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc border-lr px-lg-4 px-md-4">
                     <h5 class="maindesc-1 color-b">Real Time Tracking</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1 color-b">Real Time Tracking</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list my-lg-4 py-lg-3 mx-auto">
								    <ul>
								        <li>KPI Dashboard</li>
								        <li>Timely Intervensions</li>
								        <li class="border-0">Transperent view of consumers journey</li>
								    </ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
</section>

<section class="products-section my-7 mb-7">
   <div class="container">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/product-7.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                  <div class="products-section heading-box">
                  	<h4>MeasureSID</h4>
                    <p class="subheading">Tap into your customer's mind with a click</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="row advantage-row mt-5 px-lg-0 px-2">
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1 color-b">Real Insights</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc border-lr px-lg-4 px-md-4">
                     <h5 class="maindesc-1 color-b">Connect The Dots</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-12 p-2">
                  <div class="main-desc px-lg-4">
                     <h5 class="maindesc-1 color-b">Qualitative Data</h5>
                     <p class="maindesc-1 description">Loreum Epsom loreum Epsomloreum Epsom loreum Epsom loreum Epsom loreum Epsom</p>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="container my-4">
                    <div class="row redg-bg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row px-lg-5">
                                <div class="flex-list my-lg-4 mx-auto">
								    <ul>
								        <li>Precission marketing insights</li>
								        <li class="border-0">Explore new markets,product categories and campaigns</li>
								        <li>In-depth look into buying behaviour</li>
								        <li class="border-0">Discover untapped consumer needs</li>
								    </ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
</section> -->

<?php
get_footer();