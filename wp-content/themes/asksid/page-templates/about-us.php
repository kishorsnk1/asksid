<?php
/**
 * Template Name: About Us
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */
global $post; 
get_header();
?>
<?php //include get_template_directory().'/template-parts/inner-banner.php'; ?>
	<h5 class="floatingTitle"><?php the_title(); ?></h5>
<?php
	$banner_image_about = get_field('banner_image_about');
	$banner_text_about = get_field('banner_text_about');
?>

<main class="headerWave">
	<section class="">
		<img src="<?php echo $banner_image_about['url']; ?>" alt="<?php echo $banner_image_about['alt']; ?>" class="img-fluid moveUpBanner">
					<h1 class="moveUpBannerText"><?php echo $banner_text_about; ?></h1>
	</section>
</main>

<section class="aboutOne global">
	<div class="container">
<?php

// Check value exists.
if( have_rows('section_one') ):

    // Loop through rows.
    while ( have_rows('section_one') ) : the_row();

        // Case: Paragraph layout.
        if( get_row_layout() == 'section_one_section' ):
            $left_content = get_sub_field('left_content');
            $left_image = get_sub_field('left_image');
            $right_quote_title = get_sub_field('right_quote_title');
            $right_content = get_sub_field('right_content');
            $center_full_width_content_red_color = get_sub_field('center_full_width_content_red_color');
            // Do something...
?>
		<div class="row">
			<div class="col-sm-6">
				<div class="aboutleftone mt-5">
					<p><?php echo $left_content; ?></p>
					<img src="<?php echo $left_image['url']; ?>" alt="<?php echo $left_image['alt']; ?>" class="quaterImageView" />
				</div>
			</div>
			<div class="col-sm-6">
				<div class="aboutlefttwo">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/quotered.jpg" class=" img-fluid" />
					<span><?php echo $right_quote_title; ?></span>
					<p class="mt-5"><?php echo $right_content; ?></p>					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="aboutleftthree">
					<p class="mt-5 colorRed"><?php echo $center_full_width_content_red_color; ?></p>
				</div>
			</div>
		</div>



<?php

        endif;

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;

?>
</div>
</section>


<section class="ourLeadership global">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="sectionTitle"><?php the_field('our_leadership'); ?></h2>
			</div>
		</div>
		<div class="row">
			<?php

				// Check rows exists.
				if( have_rows('our_leaders_repeater') ):

				// Loop through rows.
				while( have_rows('our_leaders_repeater') ) : the_row();

				    // Load sub field value.
				    $leader_photo = get_sub_field('leader_photo');
				    $leader_linkedin_url = get_sub_field('leader_linkedin_url');
				    $leader_name = get_sub_field('leader_name');
				    $leader_designation = get_sub_field('leader_designation');
				    $leader_description = get_sub_field('leader_description');
				    $leader_comment = get_sub_field('leader_comment');
			?>
					<div class="col-sm-6">
						<div class="leadership">
							<div class="imageGroups">
								<img src="<?php echo $leader_photo['url']; ?>" alt="<?php echo $leader_photo['alt']; ?>" class="" />
								<a target="_blank" href="<?php echo $leader_linkedin_url; ?>"><i class="fa fa-linkedin"></i></a>
							</div>
							<h5><strong><?php echo $leader_name; ?></strong> <?php echo $leader_designation; ?></h5>
							<p><?php echo $leader_description; ?></p>
							<span class="leaderQuote colorRed"><?php echo $leader_comment; ?></span>
						</div>
					</div>
			<?php

				// End loop.
				endwhile;

				// No value.
				else :
				// Do something...
				endif;
			?>
		</div>
	</div>
</section>

<?php
	$static_map_title_about = get_field('static_map_title_about');
	$static_map_image_about = get_field('static_map_image_about');
?>
<section class="staticMap global">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="sectionTitle"><?php echo $static_map_title_about; ?></h2>
			</div>
		</div>
		
	</div>
	<img src="<?php echo $static_map_image_about['url']; ?>" alt="<?php echo $static_map_image_about['alt']; ?>" class="img-fluid" />
			
</section>


<?php

// check if the flexible content field has rows of data
if( have_rows('awards_about') ):

 	// loop through the rows of data
    while ( have_rows('awards_about') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'awards_section_about' ):
        	$section_title = get_sub_field('section_title');
?>
		<section class="awards global">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2 class="sectionTitle"><?php echo $section_title; ?></h2>
					</div>
				</div>
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('list_section') ):

			 	echo '<div class="row">';

			 	// loop through the rows of data
			    while ( have_rows('list_section') ) : the_row();

					$image = get_sub_field('image');
					$text = get_sub_field('text');
					$left_gap = get_sub_field('left_gap');
					if($left_gap == 'Yes'){
?>
				<div class="col-sm-2 d-none d-sm-block"></div>
			<?php } ?>
				<div class="col-6 col-sm-4">
					<div class="singleawardSection text-center">
						<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt']; ?>" style="max-width:100px;" />
						<h2><?php echo $text; ?></h2>
					</div>
				</div>
<?php

				endwhile;

				echo '</div>';

			endif;
?>
				</div>
			</section>
<?php
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<section class="quoteSIDdoes global">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="imageGroupsQoute">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/quotered.jpg" class=" img-fluid d-block d-sm-none d-md-block" />
					<div class="SIDDoesIT">
						<div class="row">
							<div class="col-sm-6">
								<ul class="listQuotePoints">
									<li>Intelligent conversations</li>
									<li>Quicker conversions</li>
									<li>Faster Sales</li>
								</ul>
							</div>
							<div class="col-sm-6">
								<div class="SIDDoes">
									<h2><span class="colorRed">SID</span> does it all.</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();