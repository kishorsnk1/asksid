<?php
   /**
    * Template Name: Resource
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package chek
    */
   global $post; 
   get_header();
   ?>
<?php //include get_template_directory().'/template-parts/inner-banner.php'; ?>
<h5 class="floatingTitle"><?php the_title(); ?></h5>
<?php
   // $banner_image_car = get_field('banner_image_car');
   // $banner_text_car = get_field('banner_text_car');
   ?>
<main class="headerResource">
   <div class="innerBanner">
      <img src="<?php echo get_template_directory_uri();?>/assets/images/inner_banner.png" alt="Banner" class="img-fluid d-none d-sm-block">
      <img src="<?php echo get_template_directory_uri();?>/assets/images/innerbannermobile.png" alt="Banner" class="img-fluid d-block d-sm-none">
   </div>
</main>

<?php

   // Check value exists.
   if( have_rows('picked_article') ):

       // Loop through rows.
       while ( have_rows('picked_article') ) : the_row();

           // Case: Paragraph layout.
           if( get_row_layout() == 'picked_article_section' ):
               $image = get_sub_field('image');
               $category_name = get_sub_field('category_name');
               $post_title = get_sub_field('post_title');
               $authors = get_sub_field('authors');
               $pdf_link = get_sub_field('pdf_link');
               
               // Do something...
?>
            <section class="resource-overview">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-7 col-md-7 col-12">
                        <div class="resource-overview_img">
                           <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" class="img-fluid">
                        </div>
                     </div>
                     <div class="col-lg-5 col-md-5 col-12 px-lg-5">
                        <div class="Resource-content">
                           <small class="resourceSmallText fw-700"><?php echo $category_name; ?></small>
                           <div class="Resource-head mt-lg-5 mt-3">
                              <a href="<?php echo $pdf_link['url']; ?>" target="_blank"><h2><?php echo $post_title; ?></h2></a>
                           </div>
                           <ul class="authors-list editorpick_list mt-lg-5">
                              <li>
                                 <?php if( $authors ): ?>
                                    <?php //$i=0;foreach( $authors as $user ): ?>
                                 <div class="sImage1">
                                    <?php $image = get_field('profile_pic', 'user_'.$authors['ID']); ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $authors['user_firstname']; ?>" class="img-fluid">
                                 </div>                                 
                                 <div class="sAuth">
                                    <span><strong><?php echo $authors['user_firstname'] . $authors['user_lastname']; ?></strong></span>
                                    <span class="text-capitalize"><?php echo get_field('designation', 'user_'.$authors['ID']).', '. get_field('company', 'user_'.$authors['ID']); ?></span>
                                 </div>
                                 <?php //$i++;endforeach; ?>
                                 <?php endif; ?>
                              </li>
                           </ul>
                           <div class="share-box">
                              <div class="sBlogSocial">
                                 <span>Share : </span>
                                 <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                   <i class="fa fa-facebook"></i>
                                 </a>
                                 <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php echo $post_title; ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                   <i class="fa fa-twitter"></i>
                                 </a>  
                                 <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php echo $post_title; ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                   <i class="fa fa-linkedin"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>

<?php


           endif;

       // End loop.
       endwhile;

   // No value.
   else :
       // Do something...
   endif;

?>

<section class="resourceAricle mt-lg-5 mt-4">
   <div class="container">
      <div class="row px-3">
         <div class="artilce-head">
            <h2>Article</h2>
         </div>
      </div>
      <div class="row mt-4 mb-5">

         <?php

            // Check rows exists.
            if( have_rows('article_repeat') ):

                // Loop through rows.
                while( have_rows('article_repeat') ) : the_row();

                    // Load sub field value.
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $author = get_sub_field('author');
                    $pdf_file = get_sub_field('pdf_file');
                    // Do something...
         ?>
                  <div class="col-sm-6 mb-4">
                     <div class="singleListBlog" >
                        <div class="singleListBlogImage">
                           <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" class="img-fluid" onclick="myhref('<?php echo $pdf_file['url']; ?>');">                     
                        </div>
                        <div class="singleListResourceContent">
                           <h2 onclick="myhref('<?php echo $pdf_file['url']; ?>');"><?php echo $title; ?></h2>
                           <div class="row mt-4">
                              <?php if( $authors ): ?>
                                 <?php //$i=0;foreach( $users as $user ): ?>
                                 <div class="col-12">
                              <div class="sImage1">
                                 <?php $image = get_field('profile_pic', 'user_'.$authors['ID']); ?>
                                 <img src="<?php echo $image['url']; ?>" alt="<?php echo $authors['user_firstname']; ?>" class="img-fluid">
                              </div>                                 
                              <div class="sAuth">
                                 <span><strong><?php echo $authors['user_firstname'] . $authors['user_lastname']; ?></strong></span>
                                 <span class="text-capitalize"><?php echo get_field('designation', 'user_'.$authors['ID']).', '. get_field('company', 'user_'.$authors['ID']); ?></span>
                              </div>
                              </div>
                              <?php //$i++;endforeach; ?>
                              <?php endif; ?>

                              <div class="col-12 social-mt">
                                 <div class="sBlogSocial float-right">
                                    <span>Share : </span>
                                    <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-facebook"></i>
                                    </a>
                                    <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php echo $post_title; ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-twitter"></i>
                                    </a>  
                                    <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php echo $post_title; ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-linkedin"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
         <?php
                // End loop.
                endwhile;

            // No value.
            else :
                // Do something...
            endif;
         ?>
      </div>
      <!-- <div class="row mt-2 mb-5">
        <div class="col-lg-6 col-12 mx-auto">
          <div class="text-center mb-5">
            <a href="" class="button-open-small button-black mt-3">see more articles</a>
          </div>
        </div>
      </div> -->
   </div>
</section>

<section class="resourceAricle mt-lg-5 mt-4">
   <div class="container">
      <div class="row px-3">
         <div class="artilce-head">
            <h2>Case Studies</h2>
         </div>
      </div>
      <div class="row mt-4 mb-5">

         <?php

            // Check rows exists.
            if( have_rows('case_studies_repeater') ):

                // Loop through rows.
                while( have_rows('case_studies_repeater') ) : the_row();

                    // Load sub field value.
                    $images = get_sub_field('image_case');
                    $title = get_sub_field('title');
                    $author = get_sub_field('author');
                    $pdf_file = get_sub_field('pdf_file');
                    // Do something...
         ?>
                  <div class="col-sm-6 mb-4">
                     <div class="singleListBlog" >
                        <div class="singleListBlogImage">
                           <img src="<?php echo $images['url'];?>" alt="<?php echo $images['alt'];?>" class="img-fluid" onclick="myhref('<?php echo $pdf_file['url']; ?>');">                     
                        </div>
                        <div class="singleListResourceContent">
                           <h2 onclick="myhref('<?php echo $pdf_file['url']; ?>');"><?php echo $title; ?></h2>
                           <div class="row mt-4">
                              <?php if( $authors ): ?>
                                 <?php //$i=0;foreach( $users as $user ): ?>
                                 <div class="col-12">
                              <div class="sImage1">
                                 <?php $image = get_field('profile_pic', 'user_'.$authors['ID']); ?>
                                 <img src="<?php echo $image['url']; ?>" alt="<?php echo $authors['user_firstname']; ?>" class="img-fluid">
                              </div>                                 
                              <div class="sAuth">
                                 <span><strong><?php echo $authors['user_firstname'] . $authors['user_lastname']; ?></strong></span>
                                 <span class="text-capitalize"><?php echo get_field('designation', 'user_'.$authors['ID']).', '. get_field('company', 'user_'.$authors['ID']); ?></span>
                              </div>
                              </div>
                              <?php //$i++;endforeach; ?>
                              <?php endif; ?>

                              <div class="col-12 social-mt">
                                 <div class="sBlogSocial float-right">
                                    <span>Share : </span>
                                    <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-facebook"></i>
                                    </a>
                                    <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php echo $post_title; ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-twitter"></i>
                                    </a>  
                                    <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php echo $post_title; ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-linkedin"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
         <?php
                // End loop.
                endwhile;

            // No value.
            else :
                // Do something...
            endif;
         ?>
      </div>
      <!-- <div class="row mt-2 mb-5">
        <div class="col-lg-6 col-12 mx-auto">
          <div class="text-center mb-5">
            <a href="" class="button-open-small button-black mt-3">see more articles</a>
          </div>
        </div>
      </div> -->
   </div>
</section>


<section class="resourceAricle mt-lg-5 mt-4">
   <div class="container">
      <div class="row px-3">
         <div class="artilce-head">
            <h2>Videos</h2>
         </div>
      </div>
      <div class="row mt-4 mb-5">

         <?php

            // Check rows exists.
            if( have_rows('videos_repeater') ):

                // Loop through rows.
                while( have_rows('videos_repeater') ) : the_row();

                    // Load sub field value.
                    $image = get_sub_field('image_video');
                    $title = get_sub_field('title');
                    $author = get_sub_field('author');
                    $video_link = get_sub_field('video_link');
                    // Do something...
         ?>
                  <div class="col-sm-6 mb-4">
                     <div class="singleListBlog">
                        <div class="singleListBlogImage">
                           <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" class="img-fluid" onclick="myhref('<?php echo $video_link; ?>');">                     
                        </div>
                        <div class="singleListResourceContent">
                           <h2 onclick="myhref('<?php echo $video_link; ?>');"><?php echo $title; ?></h2>
                           <div class="row mt-4">
                              <?php if( $authors ): ?>
                                 <?php //$i=0;foreach( $users as $user ): ?>
                                 <div class="col-12">
                              <div class="sImage1">
                                 <?php $image = get_field('profile_pic', 'user_'.$authors['ID']); ?>
                                 <img src="<?php echo $image['url']; ?>" alt="<?php echo $authors['user_firstname']; ?>" class="img-fluid">
                              </div>                                 
                              <div class="sAuth">
                                 <span><strong><?php echo $authors['user_firstname'] . $authors['user_lastname']; ?></strong></span>
                                 <span class="text-capitalize"><?php echo get_field('designation', 'user_'.$authors['ID']).', '. get_field('company', 'user_'.$authors['ID']); ?></span>
                              </div>
                              </div>
                              <?php //$i++;endforeach; ?>
                              <?php endif; ?>

                              <div class="col-12 social-mt">
                                 <div class="sBlogSocial float-right">
                                    <span>Share : </span>
                                    <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-facebook"></i>
                                    </a>
                                    <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php echo $post_title; ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-twitter"></i>
                                    </a>  
                                    <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php echo $post_title; ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-linkedin"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
         <?php
                // End loop.
                endwhile;

            // No value.
            else :
                // Do something...
            endif;
         ?>
      </div>
      <!-- <div class="row mt-2 mb-5">
        <div class="col-lg-6 col-12 mx-auto">
          <div class="text-center mb-5">
            <a href="" class="button-open-small button-black mt-3">see more articles</a>
          </div>
        </div>
      </div> -->
   </div>
</section>


<section class="resourceAricle mt-lg-5 mt-4">
   <div class="container">
      <div class="row px-3">
         <div class="artilce-head">
            <h2>Webinars</h2>
         </div>
      </div>
      <div class="row mt-4 mb-5">

         <?php

            // Check rows exists.
            if( have_rows('webinar_repeater') ):

                // Loop through rows.
                while( have_rows('webinar_repeater') ) : the_row();

                    // Load sub field value.
                    $image = get_sub_field('image_webinar');
                    $title = get_sub_field('title');
                    $author = get_sub_field('author');
                    $pdf_file = get_sub_field('pdf_file');
                    // Do something...
         ?>
                  <div class="col-sm-6 mb-4">
                     <div class="singleListBlog">
                        <div class="singleListBlogImage">
                           <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" class="img-fluid" onclick="myhref('<?php echo $pdf_file['url']; ?>');">                     
                        </div>
                        <div class="singleListResourceContent">
                           <h2 onclick="myhref('<?php echo $pdf_file['url']; ?>');"><?php echo $title; ?></h2>
                           <div class="row mt-4">
                              <?php if( $authors ): ?>
                                 <?php //$i=0;foreach( $users as $user ): ?>
                                 <div class="col-12">
                              <div class="sImage1">
                                 <?php $image = get_field('profile_pic', 'user_'.$authors['ID']); ?>
                                 <img src="<?php echo $image['url']; ?>" alt="<?php echo $authors['user_firstname']; ?>" class="img-fluid">
                              </div>                                 
                              <div class="sAuth">
                                 <span><strong><?php echo $authors['user_firstname'] . $authors['user_lastname']; ?></strong></span>
                                 <span class="text-capitalize"><?php echo get_field('designation', 'user_'.$authors['ID']).', '. get_field('company', 'user_'.$authors['ID']); ?></span>
                              </div>
                              </div>
                              <?php //$i++;endforeach; ?>
                              <?php endif; ?>

                              <div class="col-12 social-mt">
                                 <div class="sBlogSocial float-right">
                                    <span>Share : </span>
                                    <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-facebook"></i>
                                    </a>
                                    <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=<?php echo $post_title; ?>&url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-twitter"></i>
                                    </a>  
                                    <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php echo $post_title; ?>" target="_blank" rel="noopener" aria-label="Share on Facebook">
                                      <i class="fa fa-linkedin"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
         <?php
                // End loop.
                endwhile;

            // No value.
            else :
                // Do something...
            endif;
         ?>
      </div>
      <!-- <div class="row mt-2 mb-5">
        <div class="col-lg-6 col-12 mx-auto">
          <div class="text-center mb-5">
            <a href="" class="button-open-small button-black mt-3">see more articles</a>
          </div>
        </div>
      </div> -->
   </div>
</section>

<section class="subscribe">
  <div class="container mb-5">
     <div class="row m-0">
        <div class="col-lg-7 col-md-7 col-12">
          <div class="subscribe-head">
            <h2><?php echo get_field('subscribe_text','option'); ?></h2>
          </div>
        </div>
        <div class="col-lg-5 col-md-5 col-12">
          <div class="subscribe-box">
            <?php echo do_shortcode('[contact-form-7 id="177" title="Subscribe"]'); ?>
            <!-- <input type="email" placeholder="Enter email ID" name="email" required class="subscribe-input">
              <div class="read-more btn mt-4 float-right">
                    <a href="" class="button-open-small button-white">Subscribe</a>
              </div> -->
          </div>
        </div>
      </div>
  </div>
</section>
<?php
get_footer();?>
<script type="text/javascript">
    function myhref(web){
      //window.location.href = web;
      window.open(
        web,
        '_blank' // <- This is what makes it open in a new window.
      );
   }
</script>