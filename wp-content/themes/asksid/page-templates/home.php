<?php
/**
 * Template Name: Home Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */
global $post; 
get_header();
?>
<?php

// Check value exists.
if( have_rows('home_banner') ):

    // Loop through rows.
    while ( have_rows('home_banner') ) : the_row();
    	 // Case: Paragraph layout.
        if( get_row_layout() == 'home_banner_section' ):
        	$main_title = get_sub_field('main_title');
        	$description = get_sub_field('description');
        	$text = get_sub_field('text');
        	$book_a_demo_button_text = get_sub_field('book_a_demo_button_text');
        	$desktop_background_image = get_sub_field('desktop_background_image');
        	$mobile_background_image = get_sub_field('mobile_background_image');
        	$mobile_small_image = get_sub_field('mobile_small_image');
        	?>
<section class="homeBanner global d-none d-sm-block" style="background: url('<?php echo $desktop_background_image['url']; ?>')no-repeat; background-size: cover;">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="homeBannerContent">
					<h1><?php echo $main_title; ?></h1>
					<p><?php echo $description; ?></p>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="bookDemoText">
							<p><?php echo $text; ?></p>
						</div>
					</div>
					<div class="col-sm-6">
						<button class="button-open" data-toggle="modal" data-target="#bookDemo"><?php echo $book_a_demo_button_text; ?></button>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				
			</div>
		</div>
	</div>
</section>

<section class="homeBanner global d-block d-sm-none" style="background: url(<?php echo $mobile_background_image['url'];?>)no-repeat;">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="homeBannerContent">
					<h1><?php echo $main_title; ?></h1>
					<p><?php echo $description; ?></p>
				</div>
			</div>
			<div class="col-sm-6">
				<img src="<?php echo $mobile_small_image['url']; ?>" alt="<?php echo $mobile_small_image['alt']; ?>" class="img-fluid" />
			</div>
		</div>

				<div class="row">
					<div class="col-sm-6">
						<div class="bookDemoText">
							<p><?php echo $text; ?></p>
						</div>
					</div>
					<div class="col-sm-6">
						<button class="button-close-red"><?php echo $book_a_demo_button_text; ?></button>
					</div>
				</div>
	</div>
	
</section>
<?php
        endif;

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;
?>
<?php

// check if the flexible content field has rows of data
if( have_rows('what_can_sid_do') ):

 	// loop through the rows of data
    while ( have_rows('what_can_sid_do') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'what_can_sid_do_section' ):
        	$title = get_sub_field('title');
        	$subtitle = get_sub_field('subtitle');
        	$description = get_sub_field('description');
?>
<section class="whatcanSiddo global">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2><?php echo $title; ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">				
				<h4><?php echo $subtitle; ?></h4>
			</div>
			<div class="col-sm-6">
				<p><?php echo $description; ?></p>
			</div>
		</div>
	</div>
</section>
<?php
if( have_rows('list_serve') ):

?>
<section class="iconSectionHome global">
	<div class="container">
		<div class="row">
			<?php
				while ( have_rows('list_serve') ) : the_row();
					$image = get_sub_field('image');
					$title = get_sub_field('title');
					$subtitle = get_sub_field('subtitle');
					$button_text = get_sub_field('button_text');
					$button_link = get_sub_field('button_link');
			?>
			<div class="col-lg-3 col-md-6 col-sm-6 col-12">
				<div class="singleIconSection mb-lg-0 mb-5">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="" />
					<h2><?php echo $title; ?></h2>
					<p><?php echo $subtitle; ?></p>
					<a href="<?php echo $button_link; ?>" class="button-open-small button-black"><?php echo $button_text; ?></a>
				</div>
			</div>
			<?php
				endwhile;
				endif;
			?>
	</div>
</section>

<?php
	 endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<?php

// check if the flexible content field has rows of data
if( have_rows('how_can_sid_help_you') ):

 	// loop through the rows of data
    while ( have_rows('how_can_sid_help_you') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'how_can_sid_help_you_section' ):
        	$section_title = get_sub_field('section_title');
?>
<section class="howcansidhelp global">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="sectionTitle"><?php echo $section_title; ?></h2>
			</div>
		</div>
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('sections') ):



			 	// loop through the rows of data
			    while ( have_rows('sections') ) : the_row();

					$select_view = get_sub_field('select_view');
					$start_text = get_sub_field('start_text');
					$title = get_sub_field('title');
					$icon_image = get_sub_field('icon_image');
					$text = get_sub_field('text');
					$description = get_sub_field('description');
					$list_of_points = get_sub_field('list_of_points');
					$mobile_image_with_text = get_sub_field('mobile_image_with_text');
					$button_text = get_sub_field('button_text');
					$button_link = get_sub_field('button_link');

					if($select_view == 'left_text_right_image'){
?>
				<div class="row mt-5">
					<div class="col-sm-6">
						<div class="howcanSidContent AlignLeft">
							<div class="row">
								<div class="col-5 col-sm-6 col-md-5">
									<small><?php echo $start_text; ?></small>
									<h6><?php echo $title; ?></h6>
								</div>
								<div class="col-7 col-sm-4 col-md-3">
									<img src="<?php echo $icon_image['url']; ?>" alt="<?php echo $icon_image['alt']; ?>" style="max-width: 40px; margin-top: 10px;"/>
								</div>
							</div>
							
							<p><strong><?php echo $text; ?></strong></p>
							<p><?php echo $description; ?></p>
							<?php echo $list_of_points; ?>
							<a href="<?php echo $button_link; ?>" class="button-open-small button-black"><?php echo $button_text; ?></a>
						</div>
					</div>
					<div class="col-sm-6">
						<img class="img-fluid alignimgRight" src="<?php echo $mobile_image_with_text['url']; ?>" alt="<?php echo $mobile_image_with_text['alt']; ?>"/>
					</div>
				</div>
<?php
}else{
	?>		
				<div class="position-relative">
					<div class="rowSectionofImageText d-none d-sm-block">
						<div class="row">
							<div class="col-sm-6">
								<img class="img-fluid alignimgLeft" src="<?php echo $mobile_image_with_text['url']; ?>" alt="<?php echo $mobile_image_with_text['alt']; ?>"/>
							</div>
							<div class="col-sm-6">
								<div class="howcanSidContent AlignRight">
									<div class="row">
										<div class="col-9 col-md-6 col-sm-7">
											<img src="<?php echo $icon_image['url']; ?>" alt="<?php echo $icon_image['alt']; ?>" style="max-width: 40px; margin-top: 10px;"/>
										</div>
										<div class="col-3 col-md-6 col-sm-5">
											<small><?php echo $start_text; ?></small>
											<h6><?php echo $title; ?></h6>
										</div>
									</div>
									
									<p><strong><?php echo $text; ?></strong></p>
									<p><?php echo $description; ?> </p>
									<?php echo $list_of_points; ?>
									<a href="<?php echo $button_link; ?>" class="button-open-small button-black"><?php echo $button_text; ?></a>
								</div>
							</div>
						</div>
					</div>
					<div class="rowSectionofImageText d-block d-sm-none">
						<div class="row">				
							<div class="col-sm-6">
								<div class="howcanSidContent AlignLeft">
									<div class="row">
										<div class="col-5 col-sm-3">
											<small><?php echo $start_text; ?></small>
											<h6><?php echo $title; ?></h6>
										</div>
										<div class="col-7 col-sm-8">
											<img src="<?php echo $icon_image['url']; ?>" alt="<?php echo $icon_image['alt']; ?>" style="max-width: 40px; margin-top: 10px;"/>
										</div>							
									</div>
									
									<p><strong><?php echo $text; ?></strong></p>
									<p><?php echo $description; ?></p>
									<?php echo $list_of_points; ?>
									<a href="<?php echo $button_link; ?>" class="button-open-small button-black"><?php echo $button_text; ?></a>
								</div>
							</div>
							<div class="col-sm-6">
								<img class="img-fluid alignimgLeft" src="<?php echo $mobile_image_with_text['url']; ?>" alt="<?php echo $mobile_image_with_text['alt']; ?>"/>
							</div>
						</div>
					</div>
				</div>

	<?php
}

				endwhile;



			endif;
?>
	</div>
</section>

<?php
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<?php

// check if the flexible content field has rows of data
if( have_rows('why_sid') ):

 	// loop through the rows of data
    while ( have_rows('why_sid') ) : the_row();
?>
	<section class="whySid global d-none d-sm-block">
	<div class="container">

<?php
		// check current row layout
        if( get_row_layout() == 'why_sid_section' ):
        	$section_title = get_sub_field('section_title');
        	$subtitle = get_sub_field('subtitle');
?>
			<div class="row">
			<div class="col-sm-6">
				<div class="whySidContent">
					<h3 class="sectionTitle colorWhite"><?php echo $section_title; ?></h3>
					<p><?php echo $subtitle; ?></p>
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('options') ):
        		echo '<ul class="whySIdUl">';
			 	// loop through the rows of data
			 	$i=0;
			    while ( have_rows('options') ) : the_row();

					$option_name = get_sub_field('option_name');
					$option_description = get_sub_field('option_description');
					$icon = get_sub_field('icon');
?>
					<li>
						<a data-tab="tab-<?php echo $i; ?>" href="javascript:void(0)" class="<?php if($i==0) {echo 'active';}?>">
							<?php echo $option_name; ?>
						</a>
					</li>
<?php
				$i++;
				endwhile;
				echo '</ul>';
			endif;
?>
				</div>
				</div>
				<div class="col-sm-6">
				
				</div>
			</div>
<?php
        endif;
?>
	</div>
	<div class="quoteWhite">
		<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/quotewhite.png">
		<?php 
			if( have_rows('options') ): 
				$r=0;
				while ( have_rows('options') ) : the_row();
				$option_name = get_sub_field('option_name');
				$option_description = get_sub_field('option_description');
				$icon = get_sub_field('icon');
		?>
		<div class="quotewhiteText <?php if($r==0) {echo 'active';}?>" id="tab-<?php echo $r; ?>">
			<div class="row">
				<div class="col-10">
					<p><?php echo $option_description; ?></p>
				</div>
				<div class="col-2">
					<img class="img-fluid" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
				</div>
			</div>	
		</div>
		<?php 
			$r++;
			endwhile;
			endif; 
		?>
	</div>
</section>
<?php

    endwhile;

else :

    // no layouts found

endif;

?>

<?php

// check if the flexible content field has rows of data
if( have_rows('why_sid') ):

 	// loop through the rows of data
    while ( have_rows('why_sid') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'why_sid_section' ):
        	$section_title = get_sub_field('section_title');
        	$subtitle = get_sub_field('subtitle');
?>
<section class="whySid global d-block d-sm-none">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="whySidContent">
					<h3 class="sectionTitle colorWhite"><?php echo $section_title; ?></h3>
					<p><?php echo $subtitle; ?></p>
					
				</div>
			</div>
			<div class="col-sm-6">
				
			</div>
		</div>
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('options') ):

?>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel whysidOWL owl-theme">
<?php

			 	// loop through the rows of data
			    while ( have_rows('options') ) : the_row();

					$option_name = get_sub_field('option_name');
					$option_description = get_sub_field('option_description');
					$icon = get_sub_field('icon');
?>	
				<div class="item">
	              <h6><?php echo $option_name; ?></h6>
	              <img class="" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
	              <p><?php echo $option_description; ?></p>
	            </div>
<?php

				endwhile;

?>
			</div>
			</div>
		</div>
<?php

			endif;
?>
</div>
</section>
<?php

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>



<?php

// check if the flexible content field has rows of data
if( have_rows('measured_impact') ):

 	// loop through the rows of data
    while ( have_rows('measured_impact') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'measured_impact_section' ):
        	$section_title = get_sub_field('section_title');
?>
		<section class="mesuring global">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2 class="sectionTitle"><?php echo $section_title; ?></h2>
					</div>
				</div>
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('number_list') ):

			 	echo '<div class="row">';

			 	// loop through the rows of data
			    while ( have_rows('number_list') ) : the_row();

					$number = get_sub_field('number');
					$text = get_sub_field('text');
?>
				<div class="col-sm-4">
					<div class="numberCount">
					<div id="counter">
					 <div class="counter-value" data-count="<?php echo $number; ?>">0</div>%
					</div>
					<span><?php echo $text; ?></span>
					</div>
				</div>
<?php

				endwhile;

				echo '</div>';

			endif;

        endif;
?>
		</div>
		</section>
<?php
    endwhile;

else :

    // no layouts found

endif;

?>



<section class="largeGlobal global">
	<div class="container">
		
				<h2><?php the_field('large_global_brands_text'); ?></h2>
	</div>
	<?php $image = get_field('large_global_brands_image');?>
	<img class="img-fluid logosfloat" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
</section>


<?php

// Check value exists.
if( have_rows('extraordinary_customer') ):

    // Loop through rows.
    while ( have_rows('extraordinary_customer') ) : the_row();

        // Case: Paragraph layout.
        if( get_row_layout() == 'extraordinary_customer_section' ):
            $section_title = get_sub_field('section_title');
            $person_name_1 = get_sub_field('person_name_1');
            $person_designation_1 = get_sub_field('person_designation_1');
            $person_text_1 = get_sub_field('person_text_1');
            $person_description_1 = get_sub_field('person_description_1');
            $person_image_1 = get_sub_field('person_image_1');
            $person_name_2 = get_sub_field('person_name_2');
            $person_designation_2 = get_sub_field('person_designation_2');
            $person_text_2 = get_sub_field('person_text_2');
            $person_description_2 = get_sub_field('person_description_2');
            $person_image_2 = get_sub_field('person_image_2');
            // Do something...
?>

		<section class="extraordinary global">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2 class="sectionTitle"><?php echo $section_title; ?></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 crossline">
						<div class="row">
							<div class="col-sm-9">
								<h5><?php echo $person_name_1; ?></h5>
								<h6><?php echo $person_designation_1; ?></h6>
								<small><?php echo $person_text_1; ?></small>
							</div>
							<div class="col-sm-3">
								<img class="img-fluid" src="<?php echo $person_image_1['url']; ?>" alt="<?php echo $person_image_1['alt']; ?>">
							</div>
						</div>
						<p><?php echo $person_description_1; ?></p>
					</div>
					<div class="col-sm-1"></div>
					<div class="col-sm-5 aftercrossline">
						<div class="row">
							<div class="col-sm-9">
								<h5><?php echo $person_name_2; ?></h5>
								<h6><?php echo $person_designation_2; ?></h6>
								<small><?php echo $person_text_2; ?></small>
							</div>
							<div class="col-sm-3">
								<img class="img-fluid" src="<?php echo $person_image_2['url']; ?>" alt="<?php echo $person_image_2['alt']; ?>">
							</div>
						</div>
						<p><?php echo $person_description_2; ?></p>
					</div>
				</div>
			</div>
		</section>

       

<?php
        endif;

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;

?>


<?php

// check if the flexible content field has rows of data
if( have_rows('awards') ):

 	// loop through the rows of data
    while ( have_rows('awards') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'awards_section' ):
        	$section_title = get_sub_field('section_title');
?>
		<section class="awards global">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2 class="sectionTitle"><?php echo $section_title; ?></h2>
					</div>
				</div>
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('list_section') ):

			 	echo '<div class="row">';

			 	// loop through the rows of data
			    while ( have_rows('list_section') ) : the_row();

					$image = get_sub_field('image');
					$text = get_sub_field('text');
					$left_gap = get_sub_field('left_gap');
					if($left_gap == 'Yes'){
?>
				<div class="col-sm-2 d-none d-sm-block"></div>
			<?php } ?>
				<div class="col-6 col-sm-4">
					<div class="singleawardSection">
						<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt']; ?>" class="" />
						<h2><?php echo $text; ?></h2>
					</div>
				</div>
<?php

				endwhile;

				echo '</div>';

			endif;
?>
				</div>
			</section>
<?php
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>


<?php

// check if the flexible content field has rows of data
if( have_rows('did_you_know') ):

 	// loop through the rows of data
    while ( have_rows('did_you_know') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'did_you_know_section' ):
        	$section_title = get_sub_field('section_title');
?>
	<section class="didyouknow global">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="sectionTitle colorWhite"><?php echo $section_title; ?></h2>
				</div>
			</div>
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('testi') ):

?>
			<div class="row">
				<div class="col-12">
					<div class="owl-carousel didyouknowOWL owl-theme">
<?php

			 	// loop through the rows of data
			    while ( have_rows('testi') ) : the_row();

					$description = get_sub_field('description');
					$name = get_sub_field('name');
?>
					<div class="item">
		              <h6><?php echo $description; ?></h6>
		              <h4><?php echo $name; ?></h4>
		            </div>
<?php
				endwhile;

?>
					</div>
					</div>
				</div>
<?php

			endif;
?>
		</div>
	</section>
<?php
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<?php
	$static_map_title = get_field('static_map_title');
	$static_map_image = get_field('static_map_image');
?>
<section class="staticMap global">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="sectionTitle"><?php echo $static_map_title; ?></h2>
			</div>
		</div>
		
	</div>
	<img src="<?php echo $static_map_image['url']; ?>" alt="<?php echo $static_map_image['alt']; ?>" class="img-fluid" />
			
</section>

<section class="staticMap global">
	<div class="container">
		<?php echo get_field('intentionally_engaging_solutions');?>
		<!-- <div class="row">
			<div class="col-12">
				<h2 class="sectionTitle">Intentionally engaging solutions, seamlessly deployed in <span class="colorRed">4 weeks</span>.  </h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="greybackground">
					<h2>AskSid Chatbot</h2>
					<h2>Replay Sid</h2>
					<h2>Switch Sid</h2>
					<h2>Train Sid</h2>
					<h2>Monitor Sid</h2>
					<h2>Measure Sid</h2>
					<h2>Intelli Sid</h2>
				</div>
			</div>
		</div>
		<div class="row text-center mt-5">
			<div class="col-12">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/placeholder2.jpg" alt="" class="img-fluid" />
			</div>
		</div> -->
	</div>
</section>




<?php
get_footer();