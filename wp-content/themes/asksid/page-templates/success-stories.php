<?php
/**
 * Template Name: Sucess Stories
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */
global $post; 
get_header();
?>
<?php include(get_template_directory().'/template-parts/inner-banner.php'); ?>
	<h5 class="floatingTitle"><?php the_title(); ?></h5>

<section class="pageTitle global">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="sectionTitle"><?php the_field('page_title_success'); ?></h1>
			</div>
		</div>
	</div>
</section>

<section class="downloadSuccess global">
	<div class="container">
		<?php

			// Check rows exists.
			if( have_rows('download_section') ):

			    // Loop through rows.
			    while( have_rows('download_section') ) : the_row();

			        // Load sub field value.
			        $select_alignment = get_sub_field('select_alignment');
			        $image = get_sub_field('image');
			        $title = get_sub_field('title');
			        $download_link = get_sub_field('download_link');

			        if($select_alignment == 'left_image_right_text'){
		?>
					<div class="row mt-5">
						<div class="col-md-6">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid" />
						</div>
						<div class="col-md-5">
							<h4><?php echo $title; ?></h4>
							<a href="<?php echo $download_link; ?>" class="button-open-black" download>Download</a>
						</div>
					</div>
		<?php
		}else{
			?>
				<div class="d-none d-sm-none d-md-block">
					<div class="row mt-5 mb-4">
						<div class="offset-md-1 col-md-5 pr-0">
							<h4><?php echo $title; ?></h4>
							<a href="<?php echo $download_link; ?>" class="button-open-black" download>Download</a>
						</div>
						<div class="col-md-6 pl-0">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid ml--10" />
						</div>
					</div> 
				</div>
				<div class="d-block d-sm-block d-md-none">
					<div class="row mt-5 mb-4">
						<div class="col-md-6">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid" />
						</div>
						<div class="offset-md-1 col-md-5">
							<h4><?php echo $title; ?></h4>
							<a href="<?php echo $download_link; ?>" class="button-open-black" download>Download</a>
						</div>
					</div>
				</div>
			<?php
		}

			    // End loop.
			    endwhile;

			// No value.
			else :
			    // Do something...
			endif;
		?>
		
	</div>
</section>

<?php
get_footer();