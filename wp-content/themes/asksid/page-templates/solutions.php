<?php
   /**
    * Template Name: Solutions
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package chek
    */
   global $post; 
   get_header();
   ?>
<?php //include get_template_directory().'/template-parts/inner-banner.php'; ?>
<h5 class="floatingTitle"><?php the_title(); ?></h5>
<?php
   $banner_image_solutions = get_field('banner_image_solutions');
   $banner_text_solutions = get_field('banner_text_solutions');
   ?>
<main class="headerWave">
   <section class="">
      <img src="<?php echo $banner_image_solutions['url'];?>" alt="<?php echo $banner_image_solutions['alt'];?>" class="img-fluid moveUpBanner">
      <h1 class="moveUpBannerSolutionHead"><?php echo $banner_text_solutions; ?></h1>
   </section>
</main>



<?php

// check if the flexible content field has rows of data
if( have_rows('flexible_section_solutions') ):

  // loop through the rows of data
    while ( have_rows('flexible_section_solutions') ) : the_row();

    // check current row layout
        if( get_row_layout() == 'flexible_section_content' ):
          $image_1 = get_sub_field('image_1');
          $title = get_sub_field('title');
          $subtitle = get_sub_field('subtitle');
          $description = get_sub_field('description');
          $first_section_title = get_sub_field('first_section_title');
          $first_section_points = get_sub_field('first_section_points');
          $second_section_title = get_sub_field('second_section_title');
          $second_section_points = get_sub_field('second_section_points');
          $third_section_title = get_sub_field('third_section_title');
          $third_section_points = get_sub_field('third_section_points');
          $success_stories_to_show = get_sub_field('success_stories_to_show');
?>

          <section class="product-overview my-7">
           <div class="container mb-lg-5 mb-5">
              <div class="row">
                 <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
                    <div class="row">
                       <div class="col-lg-3 col-md-4 col-12 how-img">
                          <img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" class="img-fluid moveUpBanner">
                       </div>
                       <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                          <div class="products-section heading-box">
                            <h4><?php echo $title; ?></h4>
                            <p class="subheading"><?php echo $subtitle; ?></p>
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="col-lg-11 col-md-12 col-12 mx-auto my-4">
                   <h4 class="text-center mb-lg-5 mb-3"><?php echo $description; ?></h4>
                   <div class="row mt-3">
                    <div class="col-lg-4 col-md-4 col-12">
                      <div class="overview-box">
                        <h5 class="maindesc-1"><?php echo $first_section_title; ?></h5>
                        <?php echo $first_section_points; ?>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                      <div class="overview-box">
                        <h5 class="maindesc-1"><?php echo $second_section_title; ?></h5>
                        <?php echo $second_section_points; ?>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                      <div class="overview-box">
                        <h5 class="maindesc-1"><?php echo $third_section_title; ?></h5>
                        <?php echo $third_section_points; ?>
                      </div>
                    </div>
                   </div>

<?php
if($success_stories_to_show == '1'){
          // check if the nested repeater field has rows of data
                  if( have_rows('success_stories') ):
                // loop through the rows of data
                  while ( have_rows('success_stories') ) : the_row();

                  $image_succ = get_sub_field('image_succ');
                  $title_succ = get_sub_field('title_succ');
                  $button_link = get_sub_field('button_link');
                  
?>
                  <div class="row mt-lg-5 mt-4 bg-silver">
                     <div class="col-12 mx-auto p-5">
                      <div class="row">
                       <div class="col-lg-6 col-md-6 col-12 how-img">
                          <img src="<?php echo $image_succ['url']; ?>" alt="<?php echo $image_succ['alt']; ?>" class="img-fluid rounded">
                       </div>
                       <div class="col-lg-6 col-md-6 col-12 px-lg-5">
                          <div class="products-section heading-box">
                            <h2><?php echo $title_succ; ?> </h2>
                          </div>
                          <div class="read-more btn mt-lg-5 float-right">
                            <a href="<?php echo $button_link; ?>" class="button-open-small button-gradient">read success story</a>
                          </div>
                       </div>
                    </div>
                     </div>
                   </div>

  <?php
                endwhile;
              endif;
}else{
?>
      <div class="row mt-lg-5 mt-4">
             <div class="col-12 mx-auto p-lg-5 p-2" style="overflow: hidden;">
              <div class="row">
<?php  
        if( have_rows('success_stories') ):
                // loop through the rows of data
                  while ( have_rows('success_stories') ) : the_row();

                  $image_succ = get_sub_field('image_succ');
                  $title_succ = get_sub_field('title_succ');
                  $button_link = get_sub_field('button_link');
?>
            <div class="col-lg-4 col-md-4 col-12 mb-4">
                  <div class="bg-silver">
                     <div class="card-image p-4">
                       <img src="<?php echo $image_succ['url']; ?>" alt="<?php echo $image_succ['alt']; ?>" class="img-fluid rounded">
                     </div>
                     <div class="card-content px-4 pb-3">
                       <h2 class="card-head pb-3"><?php echo $title_succ; ?></h2>
                         <div class="read-more btn mx-auto">
                          <a href="<?php echo $button_link; ?>" class="button-open-small button-gradient p-middle">read success story</a>
                        </div>
                     </div>
                  </div>
                </div>
<?php
        endwhile;
        endif;  
?>
</div></div></div>
<?php    
}
?>
      </div>
      </div>
   </div>
   <div class="container-fluid red-wave mb-5">
     <div class="row m-0">
       <div class="col-lg-10 col-md-11 col-12 mx-auto">
         <div class="flex-listfull">
                    <ul>
                    <?php
                            // Loop over sub repeater rows.
                            if( have_rows('success_points') ):
                                while( have_rows('success_points') ) : the_row();

                                    // Get sub value.
                                    $name = get_sub_field('name');
                                    $border_right = get_sub_field('border_right');
                    ?>
                                    <li class="<?php if($border_right == 'No'){echo 'border-0'; } ?>"><?php echo $name; ?></li>
                    <?php
                                endwhile;
                            endif;


                    ?>
                      </ul>
                </div>
            </div>
       </div>
     </div>
   </div>
</section>
<?php

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>
<!-- <section class="product-overview my-7">
   <div class="container mb-lg-5 mb-5">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/solution-one.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                  <div class="products-section heading-box">
                    <h4>Customer Conversions</h4>
                    <p class="subheading">Proactively accelerating your conversions</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-11 col-md-12 col-12 mx-auto my-4">
           <h4 class="text-center mb-lg-5 mb-3">SID works towords exceeding consumer expectation in every interactions,meaning reduced abandoned carts,and more converted consumers.</h4>
           <div class="row mt-3">
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Amplifying customer acquisition</h5>
                <ul class="solution-overview">
                  <li><i class="fa fa-minus"></i>Your retail expert SID identifies and qualifies leads ensuring thier needs are alwyas met</li>
                  <li><i class="fa fa-minus"></i>Qualified leads get transferred in real-time to human agents to close the sales</li>
                  <li><i class="fa fa-minus"></i>SID and Human agents are connected through a dashboard,ensuring qualified leads are never missed</li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Proactively engaging interactions</h5>
                <ul class="solution-overview">
                  <li><i class="fa fa-minus"></i>SID gets ahead of known consumer needs by offering reccomondation to narrow down product search</li>
                  <li><i class="fa fa-minus"></i>Qualified leads get transferred in real-time to human agents to close the sales</li>
                  <li><i class="fa fa-minus"></i>SID and Human agents are connected through a dashboard,ensuring qualified leads are never missed</li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Seamless shopping across channels</h5>
                <ul class="solution-overview">
                  <li><i class="fa fa-minus"></i>Your retail expert SID identifies and qualifies leads ensuring thier needs are alwyas met</li>
                  <li><i class="fa fa-minus"></i>Qualified leads get transferred in real-time to human agents to close the sales</li>
                  <li><i class="fa fa-minus"></i>SID and Human agents are connected through a dashboard,ensuring qualified leads are never missed</li>
                </ul>
              </div>
            </div>
           </div>

           <div class="row mt-lg-5 mt-4 bg-silver">
             <div class="col-12 mx-auto p-5">
              <div class="row">
               <div class="col-lg-6 col-md-6 col-12 how-img">
                  <img src="http://localhost/asksid/wp-content/themes/asksid/assets/images/solution-box.jpg" class="img-fluid rounded">
               </div>
               <div class="col-lg-6 col-md-6 col-12 px-lg-5">
                  <div class="products-section heading-box">
                    <h2>Read more to see how SID increased conversions for one of Austria's laxury skin wear brands </h2>
                  </div>
                  <div class="read-more btn mt-lg-5 float-right">
                    <a href="" class="button-open-small button-gradient">read success story</a>
                  </div>
               </div>
            </div>
             </div>
           </div>
         </div>
      </div>
   </div>
   <div class="container-fluid red-wave mb-5">
     <div class="row m-0">
       <div class="col-lg-10 col-md-11 col-12 mx-auto">
         <div class="flex-listfull">
                    <ul>
                        <li>Accelearte conversion rate by 30%</li>
                        <li class="border-0">Automated onboarding process Automated onboarding process</li>
                        <li>Retail ontology</li>
                        <li>AI Brain customized to your brand and consumer profile</li>
                        <li class="border-0">e-commerce integration</li>
                    </ul>
                </div>
            </div>
       </div>
     </div>
   </div>
</section>


<section class="product-overview my-7">
   <div class="container mb-lg-5 mb-5">
      <div class="row">
         <div class="col-lg-7 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/solution-one.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                  <div class="products-section heading-box">
                    <h4>Customer Insights</h4>
                    <p class="subheading">Insights that drive actions</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-11 col-md-12 col-12 mx-auto my-4">
           <h4 class="text-center mb-lg-5 mb-3">SID drives millions of conversions that contain scores of untapped consumer needs,that can now be used to bring out actionable bussiness changes</h4>
           <div class="row mt-3">
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Uncovering hidden demand signals</h5>
                <ul class="solution-overview p-0">
                  <li>Your retail expert SID identifies and qualifies leads ensuring thier needs are alwyas met</li>
                  <li>Qualified leads get transferred in real-time to human agents to close the sales</li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Explore new bussiness opportunities</h5>
                <ul class="solution-overview p-0">
                  <li>SID gets ahead of known consumer needs by offering reccomondation to narrow down product search</li>
                  <li>Qualified leads get transferred in real-time to human agents to close the sales</li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Customized campaigns</h5>
                <ul class="solution-overview p-0">
                  <li>Your retail expert SID identifies and qualifies leads ensuring thier needs are alwyas met</li>
                  <li>Qualified leads get transferred in real-time to human agents to close the sales</li>
                  <li>SID and Human agents are connected through a dashboard,ensuring qualified leads are never missed</li>
                </ul>
              </div>
            </div>
           </div>

           <div class="row mt-lg-5 mt-4">
             <div class="col-12 mx-auto p-lg-5 p-2">
              <div class="row">

                <div class="col-lg-4 col-md-4 col-12 mb-4">
                  <div class="bg-silver">
                     <div class="card-image p-4">
                       <img src="http://localhost/asksid/wp-content/themes/asksid/assets/images/solution-box.jpg" class="img-fluid rounded">
                     </div>
                     <div class="card-content px-4 pb-3">
                       <h2 class="card-head pb-3">SID's insights helped a premium european retailer overcome inadequate product information,leading to increased conversions</h2>
                         <div class="read-more btn mx-auto">
                          <a href="" class="button-open-small button-gradient p-middle">read success story</a>
                        </div>
                     </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 col-12 mb-4">
                  <div class="bg-silver">
                     <div class="card-image p-4">
                       <img src="http://localhost/asksid/wp-content/themes/asksid/assets/images/solution-box.jpg" class="img-fluid rounded">
                     </div>
                     <div class="card-content px-4 pb-3">
                       <h2 class="card-head pb-3">SID's insights helped a premium european retailer overcome inadequate product information,leading to increased conversions</h2>
                         <div class="read-more btn mx-auto">
                          <a href="" class="button-open-small button-gradient p-middle">read success story</a>
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12 mb-4">
                  <div class="bg-silver">
                     <div class="card-image p-4">
                       <img src="http://localhost/asksid/wp-content/themes/asksid/assets/images/solution-box.jpg" class="img-fluid rounded">
                     </div>
                     <div class="card-content px-4 pb-3">
                       <h2 class="card-head pb-3">SID's insights helped a premium european retailer overcome inadequate product information,leading to increased conversions</h2>
                         <div class="read-more btn mx-auto">
                          <a href="" class="button-open-small button-gradient p-middle">read success story</a>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
             </div>
           </div>
         </div>
      </div>
   </div>
   <div class="container-fluid red-wave mb-5">
     <div class="row m-0">
       <div class="col-lg-10 col-md-11 col-12 mx-auto">
         <div class="flex-listfull">
                    <ul>
                        <li>Accelearte conversion rate by 30%</li>
                        <li class="border-0">Automated onboarding process Automated onboarding process</li>
                        <li>Retail ontology</li>
                        <li>AI Brain customized to your brand and consumer profile</li>
                        <li class="border-0">e-commerce integration</li>
                    </ul>
                </div>
            </div>
       </div>
     </div>
   </div>
</section>

<section class="product-overview my-7 mb-7">
   <div class="container mb-lg-5 mb-5">
      <div class="row">
         <div class="col-lg-8 col-md-10 col-12 mx-auto text-center">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12 how-img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/images/customer-support.jpg" class="img-fluid moveUpBanner">
               </div>
               <div class="col-lg-9 col-md-8 col-12 py-lg-4 py-md-5">
                  <div class="products-section heading-box">
                    <h4>Customer Support</h4>
                    <p class="subheading">Drive intelligance in every conversions,with SID</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-11 col-md-12 col-12 mx-auto my-4">
           <h4 class="text-left mb-lg-5 mb-3">Lets SID take care of challenges like repetative product or order queries,skyrocketing call volumes,and managing agent depandancy,while you concentrate on your retail bussiness goals.</h4>
           <div class="row mt-3">
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Uncovering hidden demand signals</h5>
                <ul class="solution-overview p-0">
                  <li>Your retail expert SID identifies and qualifies leads ensuring thier needs are alwyas met</li>
                  <li>Qualified leads get transferred in real-time to human agents to close the sales</li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Explore new bussiness opportunities</h5>
                <ul class="solution-overview p-0">
                  <li>SID gets ahead of known consumer needs by offering reccomondation to narrow down product search</li>
                  <li>Qualified leads get transferred in real-time to human agents to close the sales</li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
              <div class="overview-box">
                <h5 class="maindesc-1">Customized campaigns</h5>
                <ul class="solution-overview p-0">
                  <li>Your retail expert SID identifies and qualifies leads ensuring thier needs are alwyas met</li>
                  <li>Qualified leads get transferred in real-time to human agents to close the sales</li>
                  <li>SID and Human agents are connected through a dashboard,ensuring qualified leads are never missed</li>
                </ul>
              </div>
            </div>
           </div>

           <div class="row mt-lg-5 mt-4 bg-silver">
             <div class="col-12 mx-auto p-5">
              <div class="row">
               <div class="col-lg-6 col-md-6 col-12 how-img">
                  <img src="http://localhost/asksid/wp-content/themes/asksid/assets/images/solution-box.jpg" class="img-fluid rounded">
               </div>
               <div class="col-lg-6 col-md-6 col-12 px-lg-5">
                  <div class="products-section heading-box">
                    <h2>Read more to see how SID increased conversions for one of Austria's laxury skin wear brands </h2>
                  </div>
                  <div class="read-more btn mt-lg-5 float-right">
                    <a href="" class="button-open-small button-gradient">read success story</a>
                  </div>
               </div>
            </div>
             </div>
           </div>
         </div>
      </div>
   </div>
   <div class="container-fluid red-wave mb-5">
     <div class="row m-0">
       <div class="col-lg-10 col-md-11 col-12 mx-auto">
         <div class="flex-listfull">
                    <ul>
                        <li>Accelearte conversion rate by 30%</li>
                        <li class="border-0">Automated onboarding process Automated onboarding process</li>
                        <li>Retail ontology</li>
                        <li>AI Brain customized to your brand and consumer profile</li>
                        <li class="border-0">e-commerce integration</li>
                    </ul>
                </div>
            </div>
       </div>
     </div>
   </div>
</section>

<section class="solution-delivery mt-5">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12">
        <h4 class="redhead">Delivering impactful consumer experience</h4>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-lg-6 col-md-6 col-12 mb-7">
        <div class="solutions-final-block">
          <h5 class="final-head">Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom</h5>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-12 mb-7">
        <div class="finalimg-block">
          <img src="http://localhost/asksid/wp-content/themes/asksid/assets/images/blank-img.png" class="img-fluid floatR">
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-12 mb-7">
        <div class="finalimg-block">
          <img src="http://localhost/asksid/wp-content/themes/asksid/assets/images/blank-img.png" class="img-fluid floatL">
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-12 mb-7">
        <div class="solutions-final-block">
          <h5 class="final-head">Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom Loreaum epsom</h5>
        </div>
      </div>
    </div>
  </div>
</section> -->
<?php
get_footer();