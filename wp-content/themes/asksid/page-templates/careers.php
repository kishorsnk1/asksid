<?php
/**
 * Template Name: Careers
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */
global $post; 
get_header();
?>
<?php //include get_template_directory().'/template-parts/inner-banner.php'; ?>
	<h5 class="floatingTitle"><?php the_title(); ?></h5>
<?php
	$banner_image_car = get_field('banner_image_car');
	$banner_text_car = get_field('banner_text_car');
?>

<main class="headerWave">
	<section class="">
		<img src="<?php echo $banner_image_car['url']; ?>" alt="<?php echo $banner_image_car['alt']; ?>" class="img-fluid moveUpBanner">
					<h1 class="moveUpBannerText"><?php echo $banner_text_car; ?></h1>
	</section>
</main>



<?php

// check if the flexible content field has rows of data
if( have_rows('openings_car') ):

 	// loop through the rows of data
    while ( have_rows('openings_car') ) : the_row();

		// check current row layout
        if( get_row_layout() == 'openings_section' ):
        	$section_title = get_sub_field('section_title');
        	$right_section_title = get_sub_field('right_section_title');
        	$right_section_email_id = get_sub_field('right_section_email_id');
        	$right_section_image = get_sub_field('right_section_image');
        	$right_section_title_1 = get_sub_field('right_section_title_1');
        	$right_section_description = get_sub_field('right_section_description');
        	$right_section_button_text = get_sub_field('right_section_button_text');
        	$right_section_button_link = get_sub_field('right_section_button_link');
?>
	<section class="jobOpenings global">
		<div class="container">			
<?php
        	// check if the nested repeater field has rows of data
        	if( have_rows('jobs_list') ):

?>
			<div class="row">
			<div class="col-sm-6">
						<h2><?php echo $section_title; ?></h2>
<?php

			 	// loop through the rows of data
			    while ( have_rows('jobs_list') ) : the_row();

					$job_name = get_sub_field('job_name');
					$experience = get_sub_field('experience');

?>	
				<div class="jobs" data-toggle="modal" data-target="#careerPopup">
					<h4><?php echo $job_name; ?></h4>
					<h6><?php echo $experience; ?></h6>
					<span><i class="fa fa-arrow-right"></i></span>
				</div>
<?php

				endwhile;

?>
				</div>
				<div class="col-sm-6">
					<h6><?php echo $right_section_title; ?></h6>
					<a class="emailID" href="mailto:<?php echo $right_section_email_id; ?>"><?php echo $right_section_email_id; ?></a>
					<img src="<?php echo $right_section_image['url']; ?>" alt="<?php echo $right_section_image['alt']; ?>" class="img-fluid" />
					<h2 class="mt-5"><?php echo $right_section_title_1; ?></h2>
					<p><?php echo $right_section_description; ?></p>
					<a href="<?php echo $right_section_button_link; ?>" class="button-open-small button-black mt-3"><?php echo $right_section_button_text; ?></a>
				</div>
				</div>
<?php

			endif;
?>
		</div>
</section>
<?php
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<!-- The Modal -->
<div class="modal" id="careerPopup">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Join Us</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
        	<?php echo do_shortcode('[contact-form-7 id="210" title="Careers"]'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
get_footer();