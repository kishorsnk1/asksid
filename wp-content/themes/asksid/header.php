<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chek
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favi.png" type="ico/png">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

	<?php
    $header_logo = get_field('header_logo', 'option');
  ?>



  <header class="headers global ftop desktop-ftop hidden-xs d-none d-sm-block">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <nav class="navbar navbar-expand-lg p-0">
              <a class="navbar-brand m-0" href="<?php echo site_url(); ?>">
                <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" class="Header-Logo defaultt-logo" />
              </a>
              <button class="navbar-toggler navbar-hamburg" type="button" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse navbarsExample05" id="navbarsExample05">
                <?php
                wp_nav_menu(array(
                  'menu'              => 'primarymenu',
                  'theme_location'    => 'primarymenu',
                  'depth'             => 5,
                  'container'         => 'ul',
                  'list_item_class'  => 'nav-item',
                  'link_class'   => 'nav-link',
                  //'container_class'   => 'collapse navbar-collapse',
                  //'container_id'      => 'bs-example-navbar-collapse-1',
                  'menu_class'        => 'navbar-nav mr-auto align-items-center',
                  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                  'walker'            => new wp_bootstrap_navwalker()
                ));
                ?>
                <p class="form-inline my-2 my-lg-0 menuRightText">AskSid, a Techstarts portfolio company</p>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <header class="headers global ftop desktop-ftop hidden-xs d-block d-sm-none">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <p class="form-inline my-2 my-lg-0 menuRightText">AskSid, a Techstarts portfolio company</p>
            <nav class="navbar navbar-expand-lg p-0">
              <a class="navbar-brand m-0" href="<?php echo site_url(); ?>">
                <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" class="Header-Logo defaultt-logo" />
              </a>
              <button class="navbar-toggler navbar-hamburg" type="button" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse navbarsExample05" id="navbarsExample05">
                <?php
                wp_nav_menu(array(
                  'menu'              => 'primarymenu',
                  'theme_location'    => 'primarymenu',
                  'depth'             => 5,
                  'container'         => 'ul',
                  'list_item_class'  => 'nav-item',
                  'link_class'   => 'nav-link',
                  //'container_class'   => 'collapse navbar-collapse',
                  //'container_id'      => 'bs-example-navbar-collapse-1',
                  'menu_class'        => 'navbar-nav mr-auto align-items-center',
                  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                  'walker'            => new wp_bootstrap_navwalker()
                ));
                ?>
                
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <div class="slidingMenu slidingMenu_hide">
      <div class="small6">
        <a href="javascript:void(0)" class="closeMenu navbar-hamburg">X</a>
      </div>
      <div class="small6">

      </div>
      <a class="navbar-brand mobilelogomenu" href="<?php echo site_url(); ?>"><img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" /></a>
      <div class="clearfix"></div>
      <?php
      wp_nav_menu(array(
        'menu'              => 'primarymenu',
        'theme_location'    => 'primarymenu',
        'depth'             => 5,
        'container'         => 'div',
        //'container_class'   => 'collapse navbar-collapse',
        //'container_id'      => 'bs-example-navbar-collapse-1',
        'menu_class'        => 'navbar-nav ml-auto',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker()
      ));
      ?>
    </div>
  <div class="menuOverlay"></div>