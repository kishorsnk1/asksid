<?php
/**
 * chek functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package chek
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'chek_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function chek_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on chek, use a find and replace
		 * to change 'chek' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'chek', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		// register_nav_menus(
		// 	array(
		// 		'menu-1' => esc_html__( 'Primary', 'chek' ),
		// 	)
		// );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'chek_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'chek_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function chek_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'chek_content_width', 640 );
}
add_action( 'after_setup_theme', 'chek_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function chek_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'chek' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'chek' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'chek_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function chek_scripts() {
	wp_enqueue_style( 'chek-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'chek-style', 'rtl', 'replace' );

	wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/css/bootstrap.min.css');
	wp_enqueue_style('fontawesome',get_template_directory_uri().'/assets/css/font-awesome.min.css');

	wp_enqueue_style('owlcssdefualt',get_template_directory_uri().'/assets/css/owl.theme.default.min.css');
		wp_enqueue_style('owlcss',get_template_directory_uri().'/assets/css/owl.carousel.min.css');

	wp_enqueue_style('style',get_template_directory_uri().'/assets/css/style.css');
	
	//wp_deregister_script('jquery');
	wp_enqueue_script('jquery-library',get_template_directory_uri().'/assets/js/jquery.min.js',array(),false,true);
	wp_enqueue_script('popper',get_template_directory_uri().'/assets/js/popper.min.js',array(),false,true);
	wp_enqueue_script('bootstrapjs',get_template_directory_uri().'/assets/js/bootstrap.min.js',array(),false,true);
	wp_enqueue_script('jquery-validate', 'https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js',array(),false,true);
	

	wp_enqueue_script('owljs',get_template_directory_uri().'/assets/js/owl.carousel.min.js',array(),false,true);

	wp_enqueue_script('custom',get_template_directory_uri().'/assets/js/main.js',array(),false,true);
	//wp_enqueue_script( 'chek-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'chek_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

register_nav_menus( array(
	'primarymenu' => esc_html__( 'Primary Menu', 'chek' ),
) );

register_nav_menus( array(
	'footermenu1' => esc_html__( 'Footer Menu 1', 'chek' ),
) );
register_nav_menus( array(
	'footermenu2' => esc_html__( 'Footer Menu 2', 'chek' ),
) );
register_nav_menus( array(
	'footermenu3' => esc_html__( 'Footer Menu 3', 'chek' ),
) );
register_nav_menus( array(
	'footermenu4' => esc_html__( 'Footer Menu 4', 'chek' ),
) );


flush_rewrite_rules();

require_once('template-parts/wp_bootstrap_navwalker.php');


function add_menu_link_class( $atts, $item, $args ) {
  if (property_exists($args, 'link_class')) {
    $atts['class'] = $args->link_class;
  }
  return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );


function add_menu_list_item_class($classes, $item, $args) {
  if (property_exists($args, 'list_item_class')) {
      $classes[] = $args->list_item_class;
  }
  return $classes;
}
add_filter('nav_menu_css_class', 'add_menu_list_item_class', 1, 3);


function mainWidgets(){

	register_sidebar(array(
		'name'			=>	'Footer 1',
		'id'			=>	'footer1',
		'before_widget'	=>	'<div class="part-1">',
		'after_widget'	=>	'</div>'
	));

	register_sidebar(array(
		'name'			=>	'Footer 2',
		'id'			=>	'footer2',
		'before_widget'	=>	'<div class="part-2">',
		'after_widget'	=>	'</div>'
	));

	register_sidebar(array(
		'name'			=>	'Footer 3',
		'id'			=>	'footer3',
		'before_widget'	=>	'<div class="part-3">',
		'after_widget'	=>	'</div>'
	));
	
	
}

add_action('widgets_init', 'mainWidgets');


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Top Header Settings',
	// 	'menu_title'	=> 'Top Header',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Banner Settings',
		'menu_title'	=> 'Banner',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Home Settings',
		'menu_title'	=> 'Home',
		'parent_slug'	=> 'theme-general-settings',
	));

	
}

/* Function to disable the emoji's */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/* Filter function used to remove the tinymce emoji plugin. */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/* Remove emoji CDN hostname from DNS prefetching hints. */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
  /* This filter is documented in wp-includes/formatting.php */
  $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
  $urls = array_diff( $urls, array( $emoji_svg_url ) );
 }
return $urls;
}

// Count Blog open for popular blogs
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


// redirect default blog page
function wpse101952_redirect() {
  global $post;

    if(is_single('blog')) { //examples: is_home() or is_single() or is_user_logged_in() or isset($_SESSION['some_var'])

        wp_redirect('blogs');

        exit();
    }
}
add_action( 'template_redirect', 'wpse101952_redirect' );