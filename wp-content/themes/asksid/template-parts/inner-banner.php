<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package chek
 */

?>
<div class="innerBanner">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/inner_banner.png" alt="Banner" class="img-fluid d-none d-sm-block" />
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/innerbannermobile.png" alt="Banner" class="img-fluid d-block d-sm-none" />
</div>